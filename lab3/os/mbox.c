#include "ostraps.h"
#include "dlxos.h"
#include "process.h"
#include "synch.h"
#include "queue.h"
#include "mbox.h"

static mbox mboxes[MBOX_NUM_MBOXES]; 	// All mailboxes in the system
static mbox_message mboxmsgs[MBOX_NUM_BUFFERS];	// All mailbox messages in the system

extern struct PCB *currentPCB;
//-------------------------------------------------------
//
// void MboxModuleInit();
//
// Initialize all mailboxes.  This process does not need
// to worry about synchronization as it is called at boot
// time.  Only initialize necessary items here: you can
// initialize others in MboxCreate.  In other words, 
// don't waste system resources like locks and semaphores
// on unused mailboxes.
//
//-------------------------------------------------------

void MboxModuleInit() {
  int i; // Loop Index variable
  dbprintf ('p', "MboxModuleInit: Entering MboxModuleInit\n");
  for(i = 0; i < MBOX_NUM_MBOXES; i++) {
  	mboxes[i].inuse = 0;
  }
  for(i = 0; i < MBOX_NUM_BUFFERS; i++) {
  	mboxmsgs[i].inuse = 0;
  }
  dbprintf ('p', "MboxModuleInit: Leaving MboxModuleInit\n");
}

//-------------------------------------------------------
//
// mbox_t MboxCreate();
//
// Allocate an available mailbox structure for use. 
//
// Returns the mailbox handle on success
// Returns MBOX_FAIL on error.
//
//-------------------------------------------------------
mbox_t MboxCreate() {
	mbox_t mbox;
	uint32 intrval;
    int i = 0;

	// grabbing a mailbox should be an atomic operation
  	intrval = DisableIntrs();
	for(mbox=0; mbox<MBOX_NUM_MBOXES; mbox++) {
		if(mboxes[mbox].inuse==0) {
			mboxes[mbox].inuse = 1;
			break;
		}
	}
	RestoreIntrs(intrval);
  	if(mbox==MBOX_NUM_MBOXES) return MBOX_FAIL;

  	
	if((mboxes[mbox].full = SemCreate(0)) == SYNC_FAIL){
		printf("Bad sem(mbox %d full) create in process %d.\n", mbox, GetCurrentPid());
		return MBOX_FAIL;
	}
	if((mboxes[mbox].empty = SemCreate(MBOX_MAX_BUFFERS_PER_MBOX)) == SYNC_FAIL){
		printf("Bad sem(mbox %d empty) create in process %d.\n", mbox, GetCurrentPid());
		return MBOX_FAIL;
	}
	if((mboxes[mbox].mutex = LockCreate()) == SYNC_FAIL){
		printf("Bad lock(mbox %d mutex) create in process %d.\n", mbox, GetCurrentPid());
		return MBOX_FAIL;
	}
	if (AQueueInit (&(mboxes[mbox].msgs)) != QUEUE_SUCCESS) {
		printf("FATAL ERROR: could not initialize mailbox message queue in MboxCreate!\n");
		exitsim();
	}
    for (i = 0; i < MBOX_NUM_BUFFERS; i++){
        mboxes[mbox].users[i] = false;
        
    }
    mboxes[mbox].usercount = 0;
  	return mbox;
}

//-------------------------------------------------------
// 
// void MboxOpen(mbox_t);
//
// Open the mailbox for use by the current process.  Note
// that it is assumed that the internal lock/mutex handle 
// of the mailbox and the inuse flag will not be changed 
// during execution.  This allows us to get the a valid 
// lock handle without a need for synchronization.
//
// Returns MBOX_FAIL on failure.
// Returns MBOX_SUCCESS on success.
//
//-------------------------------------------------------
int MboxOpen(mbox_t handle) {
	if(handle < 0){printf("MboxOpen: Exit 1.\n");return MBOX_FAIL;}
	if(handle >= MBOX_NUM_MBOXES){printf("MboxOpen: Exit 2.\n");return MBOX_FAIL;}
	if(mboxes[handle].inuse == 0) {printf("MboxOpen: Exit 3.\n");return MBOX_FAIL;}

    mboxes[handle].users[GetCurrentPid()] = true;
    mboxes[handle].usercount++;
    return MBOX_SUCCESS;
}

//-------------------------------------------------------
//
// int MboxClose(mbox_t);
//
// Close the mailbox for use to the current process.
// If the number of processes using the given mailbox
// is zero, then disable the mailbox structure and
// return it to the set of available mboxes.
//
// Returns MBOX_FAIL on failure.
// Returns MBOX_SUCCESS on success.
//
//-------------------------------------------------------
int MboxClose(mbox_t handle) {
	if(handle < 0) {printf("MboxClose: Exit 1.\n");return MBOX_FAIL;}
	if(handle >= MBOX_NUM_MBOXES){printf("MboxClose: Exit 2.\n");return MBOX_FAIL;}
	if(mboxes[handle].inuse == 0) {printf("MboxClose: Exit 3.\n");return MBOX_FAIL;}

	if(mboxes[handle].users[GetCurrentPid()] == false){return MBOX_FAIL;}
	mboxes[handle].users[GetCurrentPid()] = false;
    mboxes[handle].usercount--;
    if(mboxes[handle].usercount == 0){
        mboxes[handle].inuse = 0;
    }
	return MBOX_SUCCESS;
}

//-------------------------------------------------------
//
// int mboxesend(mbox_t handle,int length, void* message);
//
// Send a message (pointed to by "message") of length
// "length" bytes to the specified mailbox.  Messages of
// length 0 are allowed.  The call 
// blocks when there is not enough space in the mailbox.
// Messages cannot be longer than MBOX_MAX_MESSAGE_LENGTH.
// Note that the calling process must have opened the 
// mailbox via MboxOpen.
//
// Returns MBOX_FAIL on failure.
// Returns MBOX_SUCCESS on success.
//
//-------------------------------------------------------
int MboxSend(mbox_t handle, int length, void* message) {
	Link *l;
    int i;

	if(handle < 0) {return MBOX_FAIL;}
	if(handle >= MBOX_NUM_MBOXES){return MBOX_FAIL;}
	if(mboxes[handle].inuse == 0) {return MBOX_FAIL;}
	if(length < 0 || length > MBOX_MAX_MESSAGE_LENGTH) {return MBOX_FAIL;}
	if(mboxes[handle].users[GetCurrentPid()] == false){return MBOX_FAIL;}

    //Update mailbox message queue
	if(SemHandleWait(mboxes[handle].empty) != SYNC_SUCCESS){
		printf("FATAL ERROR: bad sem wait %d in MboxSend, exiting...\n", mboxes[handle].empty);
		exitsim();
	}
	if(LockHandleAcquire(mboxes[handle].mutex) != SYNC_SUCCESS){
		printf("FATAL ERROR: bad lock aquire %d in MboxSend, exiting...\n", mboxes[handle].mutex);
		exitsim();
	}

	// Grab a mbox_message and insert the mbox_message to mailbox
	for(i=0; i<MBOX_NUM_BUFFERS; i++) {
		if(mboxmsgs[i].inuse==0) {
			mboxmsgs[i].inuse = 1;
			break;
		}
	}
	if(i == MBOX_NUM_BUFFERS) {
		printf("Can't find process %d in mailbox %d user queue in MboxSend!\n", GetCurrentPid(), handle);
		return MBOX_FAIL;
	}
    //deep copy message and insert the message to the message queue
	dstrncpy(mboxmsgs[i].msg, (char *)message, length);
	mboxmsgs[i].pid = GetCurrentPid();
	if ((l = AQueueAllocLink ((void *)&mboxmsgs[i])) == NULL) {
      printf("FATAL ERROR: could not allocate link for mailbox users queue in MboxOpen!\n");
      exitsim();
    }
    if (AQueueInsertLast (&(mboxes[handle].msgs), l) != QUEUE_SUCCESS) {
      printf("FATAL ERROR: could not insert new link into users queue in MboxOpen!\n");
      exitsim();
    }

    if(LockHandleRelease(mboxes[handle].mutex) != SYNC_SUCCESS){
    	printf("FATAL ERROR: bad lock release %d in MboxSend, exiting...\n", mboxes[handle].mutex);
		exitsim();
    }
    if(SemHandleSignal(mboxes[handle].full) != SYNC_SUCCESS){
    	printf("Bad sem signal %d in MboxSend, exiting...\n", mboxes[handle].full);
		exitsim();
    }
    return MBOX_SUCCESS;

}

//-------------------------------------------------------
//
// int MboxRecv(mbox_t handle, int maxlength, void* message);
//
// Receive a message from the specified mailbox.  The call 
// blocks when there is no message in the buffer.  Maxlength
// should indicate the maximum number of bytes that can be
// copied from the buffer into the address of "message".  
// An error occurs if the message is larger than maxlength.
// Note that the calling process must have opened the mailbox 
// via MboxOpen.
//   
// Returns MBOX_FAIL on failure.
// Returns number of bytes written into message on success.
//
//-------------------------------------------------------
int MboxRecv(mbox_t handle, int maxlength, void* message) {
	Link *l;
	mbox_message * mmsg = 0;
	if(handle < 0) {return MBOX_FAIL;}
	if(handle >= MBOX_NUM_MBOXES) {return MBOX_FAIL;}
	if(mboxes[handle].inuse == 0) {return MBOX_FAIL;}
	if(maxlength < 0 || maxlength > MBOX_MAX_MESSAGE_LENGTH) {return MBOX_FAIL;}
	if(mboxes[handle].users[GetCurrentPid()] == false) {return MBOX_FAIL;}

    //Update mailbox message queue
	if(SemHandleWait(mboxes[handle].full) != SYNC_SUCCESS){
		printf("FATAL ERROR: bad sem wait %d in MboxRecv, exiting...\n", mboxes[handle].full);
		exitsim();
	}
	if(LockHandleAcquire(mboxes[handle].mutex) != SYNC_SUCCESS){
		printf("FATAL ERROR: bad lock aquire %d in MboxRecv, exiting...\n", mboxes[handle].mutex);
		exitsim();
	}

	// Remove the first mbox_message
    l = AQueueFirst(&(mboxes[handle].msgs));
    dstrncpy((char *)message, (char *)AQueueObject(l), maxlength);
    mmsg = (mbox_message *)AQueueObject(l);
    mmsg -> inuse = 0;
    if (AQueueRemove(&l) != QUEUE_SUCCESS) { 
      printf("FATAL ERROR: could not remove link from lock queue in MboxRecv!\n");
      exitsim();
    }

    if(LockHandleRelease(mboxes[handle].mutex) != SYNC_SUCCESS){
    	printf("FATAL ERROR: bad lock release %d in MboxRecv, exiting...\n", mboxes[handle].mutex);
		exitsim();
    }
    if(SemHandleSignal(mboxes[handle].empty) != SYNC_SUCCESS){
    	printf("Bad sem signal %d in MboxRecv, exiting...\n", mboxes[handle].full);
		exitsim();
    }

    return MBOX_SUCCESS;

}


//--------------------------------------------------------------------------------
// 
// int MboxCloseAllByPid(int pid);
//
// Scans through all mailboxes and removes this pid from their "open procs" list.
// If this was the only open process, then it makes the mailbox available.  Call
// this function in ProcessFreeResources in process.c.
//
// Returns MBOX_FAIL on failure.
// Returns MBOX_SUCCESS on success.
//
//--------------------------------------------------------------------------------
int MboxCloseAllByPid(int pid) {
	int i;
	uint32 intrval;
	Link *l;
	if(pid < 0) {return MBOX_FAIL;}
	intrval = DisableIntrs();
	for(i = 0; i < MBOX_NUM_MBOXES; i++){
		if(mboxes[i].inuse && mboxes[i].usercount > 0 && mboxes[i].users[pid]) {
            mboxes[i].users[pid] = false;
            mboxes[i].usercount--;

            if(mboxes[i].usercount == 0){
                mboxes[i].inuse = 0;
            }
		}
	}
	RestoreIntrs(intrval);
	return MBOX_SUCCESS;
}
