#include "usertraps.h"
#include "misc.h"

void main (int argc, char *argv[])
{
  mbox_t H2O;
  mbox_t H2;
  mbox_t O2;
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  char rH2O[4];
  char pH2[] = "H2";
  char pO2[] = "O2";

  if (argc != 5) { 
    Printf("Usage: "); Printf(argv[0]); Printf("<H2O mailbox> <H2 mailbox> <O2 mailbox> <number of processes semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  H2O = dstrtol(argv[1], NULL, 10);
  H2 = dstrtol(argv[2], NULL, 10);
  O2 = dstrtol(argv[3], NULL, 10);
  s_procs_completed = dstrtol(argv[4], NULL, 10);

  // Produce "Hello world" to buffer
  if(mbox_open(H2O) != MBOX_SUCCESS) {
    Printf("reactionH2O %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_open(H2) != MBOX_SUCCESS) {
    Printf("reactionH2O %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_open(O2) != MBOX_SUCCESS) {
    Printf("reactionH2O %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if (mbox_recv(H2O, sizeof(rH2O), (void *)rH2O) == MBOX_FAIL) {
    Printf("reactionH2O %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  if (mbox_recv(H2O, sizeof(rH2O), (void *)rH2O) == MBOX_FAIL) {
    Printf("reactionH2O %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  Printf("reactionH2O: 2 H2O are received!\n");
  if (mbox_send(H2, sizeof(pH2), (void *)pH2) == MBOX_FAIL) {
    Printf("reactionH2O %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  if (mbox_send(H2, sizeof(pH2), (void *)pH2) == MBOX_FAIL) {
    Printf("reactionH2O %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  if (mbox_send(O2, sizeof(pO2), (void *)pO2) == MBOX_FAIL) {
    Printf("reactionH2O %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  Printf("reactionH2O %d: 2 H2 and 1 O2 are sent!\n", getpid());
  if(mbox_close(H2O) != MBOX_SUCCESS) {
    Printf("reactionH2O %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_close(H2) != MBOX_SUCCESS) {
    Printf("reactionH2O %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_close(O2) != MBOX_SUCCESS) {
    Printf("reactionH2O %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
}

