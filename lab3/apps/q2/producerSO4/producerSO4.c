#include "usertraps.h"
#include "misc.h"

void main (int argc, char *argv[])
{
  mbox_t SO4;
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  char message[] = "SO4";

  if (argc != 3) { 
    Printf("Usage: "); Printf(argv[0]); Printf("<SO4 mailbox> <number of processes semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  SO4 = dstrtol(argv[1], NULL, 10);
  s_procs_completed = dstrtol(argv[2], NULL, 10);
 
   // Open the mailbox
  if (mbox_open(SO4) == MBOX_FAIL) {
    Printf("producerH2O %d: Could not open the mailbox!\n", getpid());
    Exit();
  }

  // Wait for a message from the mailbox
  if (mbox_send(SO4, sizeof(message), (void *)&message) == MBOX_FAIL) {
    Printf("producerSO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
 
  // Now print a message to show that one H2O has been sent
  Printf("producerSO4 %d: Send 1 SO4\n", getpid());
  if (mbox_close(SO4) != MBOX_SUCCESS) {
    Printf("producerH2O %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("producerSO4 %d: Bad semaphore s_procs_completed (%d)!\n", getpid(), s_procs_completed);
    Exit();
  }

  Printf("producerSO4 %d: Done!\n", getpid());
}
