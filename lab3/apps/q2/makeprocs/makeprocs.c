#include "usertraps.h"
#include "misc.h"
#include "reaction.h"

void main (int argc, char *argv[])
{
  int numH2O = 0;                 // Used to store number of H2O to produce 
  int numSO4 = 0;                 // Used to store number of SO4 to produce
  int min = 0;                    // Minimum value of number of SO2, number of H2 and number of H2O molecules
  int tmp = 0;
  int total = 0;
  int i = 0; // loop index
  sem_t s_procs_completed;
  mbox_t H2O;                  // Semaphore used to signal production of H2O
  mbox_t SO4;                  // Semaphore used to signal production of SO4
  mbox_t H2;                   // Semaphore used to signal creation and consumption of H2
  mbox_t O2;                   // Semaphore used to signal creation and consumption of O2
  mbox_t SO2;                  // Semaphore used to singal creation and consumption of SO2
  mbox_t H2SO4;                // Semaphore used to singal creation of H2SO4
  char s_procs_completed_str[10];  // Used as command-line argument to pass page_mapped handle to new processes
  char H2O_str[10]; //Used as command-line argument to pass page_mapped handle to new processes
  char SO4_str[10]; //Used as command-line argument to pass page_mapped handle to new processes
  char H2_str[10]; //Used as command-line argument to pass page_mapped handle to new processes
  char O2_str[10]; //Used as command-line argument to pass page_mapped handle to new processes
  char SO2_str[10]; //Used as command-line argument to pass page_mapped handle to new processes
  char H2SO4_str[10]; //Used as command-line argument to pass page_mapped handle to new processes

  if (argc != 3) {
    Printf("Usage: "); Printf(argv[0]); Printf(" <number of H2O molecules> <number of SO4 molecules>\n");
    Exit();
  }

  // Convert string from ascii command line argument to integer number
  numH2O = dstrtol(argv[1], NULL, 10); // the "10" means base 10
  numSO4 = dstrtol(argv[2], NULL, 10);

  //Calculate how many consumable molecules for H2SO4 creation
  
  if ((H2O = mbox_create()) == MBOX_FAIL) {
    Printf("Bad mbox_create(H2O) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  if ((SO4 = mbox_create()) == MBOX_FAIL) {
    Printf("Bad mbox_create(SO4) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  if ((H2 = mbox_create()) == MBOX_FAIL) {
    Printf("Bad mbox_create(H2) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  if ((O2 = mbox_create()) == MBOX_FAIL) {
    Printf("Bad mbox_create(O2) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  if ((SO2 = mbox_create()) == MBOX_FAIL) {
    Printf("Bad mbox_create(SO2) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  if ((H2SO4 = mbox_create()) == MBOX_FAIL) {
    Printf("Bad mbox_create(H2SO4) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  
  //Open all mailbox
  if (mbox_open(H2O) == MBOX_FAIL) {
    Printf("Bad mbox_open(H2O) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  if (mbox_open(SO4) == MBOX_FAIL) {
    Printf("Bad mbox_open(SO4) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }

  if (mbox_open(H2) == MBOX_FAIL) {
    Printf("Bad mbox_open(H2) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }

  if (mbox_open(O2) == MBOX_FAIL) {
    Printf("Bad mbox_open(O2) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }

  if (mbox_open(SO2) == MBOX_FAIL) {
    Printf("Bad mbox_open(SO2) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }
  if (mbox_open(H2SO4) == MBOX_FAIL) {
    Printf("Bad mbox_open(H2SO4) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }

  // Create semaphore to not exit this process until all other processes 
  // have signalled that they are complete. Once each of the processes 
  // has signaled, the semaphore should be back to
  // zero and the final sem_wait below will return.
  tmp = numH2O / 2 * 2;
  tmp = (tmp > numSO4) ? numSO4 : tmp;
  min = (tmp > (numH2O /2 + numSO4)) ? (numH2O / 2 + numSO4) : tmp;
  total = numH2O + numSO4 + numH2O / 2 + numSO4 + min - 1;
  
  if ((s_procs_completed = sem_create(-total)) == SYNC_FAIL){
    Printf("Bad sem_create(s_procs_completed) in "); Printf(argv[0]); Printf("\n");
    Exit();
  }

  // Setup the command-line arguments for the new process.  We're going to
  // pass the handles to the shared memory page and the semaphore as strings
  // on the command line, so we must first convert them from ints to strings.
  ditoa(s_procs_completed, s_procs_completed_str);
  ditoa(H2O, H2O_str);
  ditoa(SO4, SO4_str);
  ditoa(H2, H2_str);
  ditoa(O2, O2_str);
  ditoa(SO2, SO2_str);
  ditoa(H2SO4, H2SO4_str);
  // Now we can create the processes.  Note that you MUST end your call to
  // process_create with a NULL argument so that the operating system
  // knows how many arguments you are sending.
  for(i = 0; i < numH2O; i++){
    process_create(PRODUCERH2O, 0, 0, H2O_str, s_procs_completed_str, NULL);
  }
  for(i = 0; i < numSO4; i++){
    process_create(PRODUCERSO4, 0, 0, SO4_str, s_procs_completed_str, NULL);
  }
  for(i = 0; i < numH2O / 2; i++){
    process_create(REACTIONH2O, 0, 0, H2O_str, H2_str, O2_str, s_procs_completed_str, NULL);
  }
  for(i = 0; i < numSO4; i++){
    process_create(REACTIONSO4, 0, 0, SO4_str, SO2_str, O2_str, s_procs_completed_str, NULL);
  }
  for(i = 0; i < min; i++){
    process_create(REACTIONH2SO4, 0, 0, H2_str, O2_str, SO2_str, H2SO4_str, s_procs_completed_str, NULL);
  }

  // And finally, wait until all spawned processes have finished.
  if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf("\n");
    Exit();
  }
  Printf("All other processes completed, exiting main process.\n");
}
