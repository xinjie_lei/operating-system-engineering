#include "usertraps.h"
#include "misc.h"

void main (int argc, char *argv[])
{
  mbox_t SO2;
  mbox_t H2;
  mbox_t O2;
  mbox_t H2SO4;
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  char rSO2[4];
  char rH2[3];
  char rO2[3];
  char pH2SO4[] = "H2SO4";


  if (argc != 6) { 
    Printf("Usage: "); Printf(argv[0]); Printf("<SO2 mbox> <H2 mbox> <O2 mbox> <H2SO4 mbox> <number of processes semaphore>.\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  SO2 = dstrtol(argv[1], NULL, 10);
  H2 = dstrtol(argv[2], NULL, 10);
  O2 = dstrtol(argv[3], NULL, 10);
  H2SO4 = dstrtol(argv[4], NULL, 10);
  s_procs_completed = dstrtol(argv[5], NULL, 10);
 
  // Produce "Hello world" to buffer
  if(mbox_open(SO2) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_open(H2) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_open(O2) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_open(H2SO4) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if (mbox_recv(SO2, sizeof(rSO2), (void *)rSO2) == MBOX_FAIL) {
    Printf("reactionH2SO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  if (mbox_recv(H2, sizeof(rH2), (void *)rH2) == MBOX_FAIL) {
    Printf("reactionH2SO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
   if (mbox_recv(O2, sizeof(rO2), (void *)rO2) == MBOX_FAIL) {
    Printf("reactionH2SO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  Printf("reactionH2SO4 %d: 1 SO2, 1 H2, and 1 O2 are received!\n", getpid());
  if (mbox_send(H2SO4, sizeof(pH2SO4), (void *)pH2SO4) == MBOX_FAIL) {
    Printf("reactionH2SO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  Printf("reactionH2SO4 %d:  1 H2SO4 is sent!\n", getpid());

   if(mbox_close(SO2) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_close(H2) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_close(O2) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_close(H2SO4) != MBOX_SUCCESS) {
    Printf("reactionH2SO4 %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
}

