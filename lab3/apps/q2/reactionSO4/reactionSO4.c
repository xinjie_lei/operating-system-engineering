#include "usertraps.h"
#include "misc.h"

void main (int argc, char *argv[])
{
  mbox_t SO4;
  mbox_t SO2;
  mbox_t O2;
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  char rSO4[4];
  char pSO2[] = "H2";
  char pO2[] = "O2";

  if (argc != 5) { 
    Printf("Usage: "); Printf(argv[0]); Printf(" <number of SO4 to be consumed> <SO4 semaphore> <SO2 semaphore> <O2 semaphore> <number of processes semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  SO4 = dstrtol(argv[1], NULL, 10);
  SO2 = dstrtol(argv[2], NULL, 10);
  O2 = dstrtol(argv[3], NULL, 10);
  s_procs_completed = dstrtol(argv[4], NULL, 10);
 
  // Produce "Hello world" to buffer
  if(mbox_open(SO4) != MBOX_SUCCESS) {
    Printf("reactionSO4 %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_open(SO2) != MBOX_SUCCESS) {
    Printf("reactionSO4 %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_open(O2) != MBOX_SUCCESS) {
    Printf("reactionSO4 %d: Could not open the mailbox!\n", getpid());
    Exit();
  }
  if (mbox_recv(SO4, sizeof(rSO4), (void *)rSO4) == MBOX_FAIL) {
    Printf("reactionSO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  Printf("reactionSO4 %d: 1 SO4 is received!\n", getpid());
  if (mbox_send(SO2, sizeof(pSO2), (void *)pSO2) == MBOX_FAIL) {
    Printf("reactionSO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  if (mbox_send(O2, sizeof(pO2), (void *)pO2) == MBOX_FAIL) {
    Printf("reactionSO4 %d: Could not map the virtual address to the memory!\n", getpid());
    Exit();
  }
  Printf("reactionSO4 %d: 1 SO2 and 1 O2 are sent!\n", getpid());
   if(mbox_close(SO4) != MBOX_SUCCESS) {
    Printf("reactionSO4 %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_close(SO2) != MBOX_SUCCESS) {
    Printf("reactionSO4 %d: Could not close the mailbox!\n", getpid());
    Exit();
  }
  if(mbox_close(O2) != MBOX_SUCCESS) {
    Printf("reactionSO4 %d: Could not close the mailbox!\n", getpid());
    Exit();
  }

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
}

