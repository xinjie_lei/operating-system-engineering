#ifndef __DFS_SHARED__
#define __DFS_SHARED__

typedef struct dfs_superblock {
  // STUDENT: put superblock internals here
  int valid;
  int blk_size;
  int blk_num;
  int inode_blk_start;
  int inode_num;
  int inode_blk_num;
  int fbv_blk_start;
  int fbv_blk_num;
  int data_blk_start;
  int data_blk_num;
} dfs_superblock;

#define DFS_BLOCKSIZE 1024  // Must be an integer multiple of the disk blocksize
#define DFS_MAX_FILENAME_LENGTH 72

typedef struct dfs_block {
  char data[DFS_BLOCKSIZE];
} dfs_block;

typedef struct dfs_inode {
  // STUDENT: put inode structure internals here
  // IMPORTANT: sizeof(dfs_inode) MUST return 96 in order to fit in enough
  // inodes in the filesystem (and to make your life easier).  To do this, 
  // adjust the maximumm length of the filename until the size of the overall inode 
  // is 96 bytes.
  unsigned char type; 		// directory or regular file
  unsigned char permission; 	// file permission
  int ownerid;  
  int inuse;

  // number of blocks used by this inode
  uint32 num_blocks;
  // number of directory entries stored in this inode
  uint32 num_entries;    
  // size of the file in bytes
  uint32 size;                             
  // direct addressesing to data blocks
  uint32 direct[10];                    
  // indirect addressing to data blocks
  uint32 indirect;                      
  // used to set inode size equal to 96 bytes
  char junk[34];
} dfs_inode;

typedef struct dfs_dentry {
	char filename[DFS_MAX_FILENAME_LENGTH];
	int inode;
} dfs_dentry;

typedef int inode_t;

#define DFS_MAX_FILESYSTEM_SIZE 0x1000000  // 16MB
#define DFS_FBV_MAX_NUM_WORDS (DFS_MAX_FILESYSTEM_SIZE/DFS_BLOCKSIZE/32)
#define DFS_NUM_INODES 192
#define DFS_FAIL -1
#define DFS_SUCCESS 1

#define TRUE	1
#define FALSE   0
#define R 1
#define W 2
#define RW 3
#define DE 4 // file delete
#define MK 5 // Make directory mode
#define RM 6 // Remove directory mode
#define OR 4
#define OW 2
#define OX 1
#define UR 32
#define UW 16
#define UX 8
#define OK 1
#define FILE 1
#define DIR 2


#define INVALID_FILE		-1
#define INVALID_INODE		-1
#define FILE_EXISTS		-2
#define DOES_NOT_EXIST		-3
#define DIRECTORY_EXISTS	-4
#define NOT_A_DIRECTORY		-5
#define NOT_A_FILE		-6
#define INVALID_PATH		-7
#define PERMISSION_DENIED	-8
#define DIRECTORY_NOT_EMPTY	-9

#endif
