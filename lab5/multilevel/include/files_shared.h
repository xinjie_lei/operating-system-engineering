#ifndef __FILES_SHARED__
#define __FILES_SHARED__

#define FILE_SEEK_SET 1
#define FILE_SEEK_END 2
#define FILE_SEEK_CUR 3

#define FILE_MAX_FILENAME_LENGTH 72
#define FILE_MAX_READWRITE_BYTES 4096

typedef struct filetab
{
	int inuse;
  uint32 curpos;            // file's current position
  int mode;                  // R/W/RW
  uint32 id;                 // owner id;
  int inode;              // pointer to the inode in memory
  int eof;                // end of file flag
  char filename[FILE_MAX_FILENAME_LENGTH];
}filetab;

#define FILE_FAIL -1
#define FILE_EOF -1
#define FILE_SUCCESS 1

#define TRUE	1
#define FALSE   0
#define R 1
#define W 2
#define RW 3
#define DE 4 // file delete
#define MK 5 // Make directory mode
#define RM 6 // Remove directory mode
#define OR 4
#define OW 2
#define OX 1
#define UR 32
#define UW 16
#define UX 8
#define OK 1
#define FILE 1
#define DIR 2


#define INVALID_FILE		-1
#define INVALID_INODE		-1
#define FILE_EXISTS		-2
#define DOES_NOT_EXIST		-3
#define DIRECTORY_EXISTS	-4
#define NOT_A_DIRECTORY		-5
#define NOT_A_FILE		-6
#define INVALID_PATH		-7
#define PERMISSION_DENIED	-8
#define DIRECTORY_NOT_EMPTY	-9

#endif
