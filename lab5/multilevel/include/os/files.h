#ifndef __FILES_H__
#define __FILES_H__

#include "dfs.h"
#include "files_shared.h"

#define FILE_MAX_OPEN_FILES 15
#define FILE_MAX_READWRITE_BYTES 4096
void FileModuleInit(void);
int FileOpen(char *filename, char *mode);
int FileClose(int handle);
int FileRead(int handle, void *mem, int num_bytes);
int FileWrite(int handle, void *mem, int num_bytes);
int FileSeek(int handle, int num_bytes, int from_where);
int FileDelete(char *filename);
int FileSetMode(int handle, char * mode);
int MkDir(char *path, int permission);
int RmDir(char *path);
int convertModeToInt(char *mode);
#endif
