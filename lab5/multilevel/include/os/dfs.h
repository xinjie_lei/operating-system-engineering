#ifndef __DFS_H__
#define __DFS_H__

#include "dfs_shared.h"
#include "files.h"

void DfsModuleInit();
void DfsInvalidate();
int DfsOpenFileSystem();
int DfsCloseFileSystem();
uint32 DfsAllocateBlock();
int DfsFreeBlock(uint32 blocknum);
int DfsReadBlock(uint32 blocknum, dfs_block *b);
int DfsWriteBlock(uint32 blocknum, dfs_block *b);
int DfsInodeCheckPath(char *path, int mode, int type, inode_t *handle);
int DfsInodeCheckDirEmpty(uint32 child);
uint32 DfsInodeSearch(inode_t parent, char *filename);
uint32 DfsInodeOpen(char type, char permission);
int DfsInodeAddLeafToParent();
int DfsInodeFreeAllDataBlocks(uint32 handle);
int DfsInodeDelete();
int DfsInodeReadBytes(uint32 handle, void *mem, int start_byte, int num_bytes);
int DfsInodeWriteBytes(uint32 handle, void *mem, int start_byte, int num_bytes);
uint32 DfsInodeFilesize(uint32 handle);
uint32 DfsInodeAllocateVirtualBlock(uint32 handle, uint32 virtual_blocknum);
uint32 DfsInodeTranslateVirtualToFilesys(uint32 handle, uint32 virtual_blocknum);
int SetupRootDir();
dfs_inode *getInodeAddress(inode_t id);
int getOneName(char *dst, char *src);
int checkPermission(inode_t id, char mode);
int bncmp(char *s1, char *s2, int num);
#endif
