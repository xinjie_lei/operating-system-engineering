#include "ostraps.h"
#include "dlxos.h"
#include "process.h"
#include "dfs.h"
#include "files.h"
#include "synch.h"

// STUDENT: put your file-level functions here
static filetab fd[FILE_MAX_OPEN_FILES];
static lock_t mutex;

// STUDENT: put your file-level functions here
void FileModuleInit(){
	int i = 0;
	for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
		fd[i].inuse = 0;
		fd[i].inode = 0;
		fd[i].curpos = 0;
		fd[i].id = 0;
		fd[i].eof = 0;
		fd[i].mode = 0;
		bzero(fd[i].filename, sizeof(fd[i].filename));
	}
	mutex = LockCreate();
}

int FileOpen(char *filename, char *mode){
	int i = 0;
	inode_t inode = 0;
	int md = convertModeToInt(mode);
	if(md == R || md == RW){
		if(DfsInodeCheckPath(filename, md, FILE, &inode) != OK){return FILE_FAIL;}
	}

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}

	for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
		if(fd[i].inuse && (dstrncmp(filename, fd[i].filename, FILE_MAX_FILENAME_LENGTH) == 0)){
					printf("FileOpen tries to open a opened file.\n");
					return FILE_FAIL;
		}
	}

	for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
		if(!fd[i].inuse){
			break;
		}
	}
	if(i == FILE_MAX_OPEN_FILES){
		printf("FileOpen couldn't find free file descripter.\n");
		return FILE_FAIL;
	}
	if(FileSetMode(i, mode) == FILE_FAIL){
		printf("FileOpen couldn't set file mode.\n");
		return FILE_FAIL;
	}

	if(md == R || md == RW){
		fd[i].inode = inode;
	}
	if(md == W){
		if(DfsInodeCheckPath(filename, md, FILE, &inode) == DOES_NOT_EXIST){
			if((fd[i].inode = DfsInodeOpen((char)FILE, (char)(UX+UR+UW))) == DFS_FAIL){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				return FILE_FAIL;
			}
			if(DfsInodeAddLeafToParent() == DFS_FAIL){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				return FILE_FAIL;
			}
		}
		else{
			fd[i].inode = inode;
		}
	}
	if(md == W || md == RW){
		if(DfsInodeFreeAllDataBlocks(fd[i].inode) == DFS_FAIL){
			if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
			return FILE_FAIL;
		}
	}
	dstrcpy(fd[i].filename, filename);
	fd[i].inuse = 1;
	fd[i].curpos = 0;
	fd[i].eof = 0;
	fd[i].id = GetCurrentPid();
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
	return i;
}

int FileClose(int handle){
	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(!fd[handle].inuse){return FILE_FAIL;}
	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}

  fd[handle].inuse = 0;
  bzero(fd[handle].filename, sizeof(fd[handle].filename));
  fd[handle].curpos = 0;
  fd[handle].mode = 0;
  fd[handle].eof = 0;
  fd[handle].id = 0;
  fd[handle].inode = 0;
  if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  return FILE_SUCCESS;
}

int FileDelete(char *path){
	int i = 0;
	inode_t inode;

	if(DfsInodeCheckPath(path, DE, FILE, &inode) != OK){
		return FILE_FAIL;
	}
	
	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
	for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
		if(fd[i].inode == inode && fd[i].inuse){
			if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
			return FILE_FAIL;
		}
	}
  
  if(DfsInodeDelete() == DFS_FAIL){
  	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  	return FILE_FAIL;
  }
  if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  return FILE_SUCCESS;
}

int MkDir(char *path, int permission){
	inode_t inode;
	if(DfsInodeCheckPath(path, MK, DIR, &inode) != OK){
		printf("MkDir couldn't make directory %s.\n", path);
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;}
	if(DfsInodeOpen((char)DIR, (char)permission) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;}
	if(DfsInodeAddLeafToParent() == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}
	return FILE_SUCCESS;
}

int RmDir(char *path){
	inode_t inode;
	if(*path == '/' && *(path++) == '\0') {return FILE_FAIL;}
	if(DfsInodeCheckPath(path, RM, DIR, &inode) != OK){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;}
	if(DfsInodeDelete() == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;}
	return FILE_SUCCESS;
}

int FileRead(int handle, void *mem, int num_bytes){
	int fsize = 0;
	int bytes_read = 0;
	
	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(num_bytes > 4096){return FILE_FAIL;}

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
	if(!fd[handle].inuse){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}

	if(fd[handle].mode != R && fd[handle].mode != RW){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		printf("FileRead file mode is not read.\n");
		return FILE_FAIL;
	}

	if(fd[handle].eof){
  	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  	printf("FileRead reaches end of file.\n");
  	return FILE_FAIL;
  }

	fsize = DfsInodeFilesize(fd[handle].inode);
	if(fd[handle].curpos + num_bytes > fsize){
		num_bytes = fsize - fd[handle].curpos;
		fd[handle].eof = 1;
	}
	if(fd[handle].curpos + num_bytes == fsize){
		fd[handle].eof = 1;
	}
	if((bytes_read = DfsInodeReadBytes(fd[handle].inode, mem, fd[handle].curpos, num_bytes)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}

	fd[handle].curpos += num_bytes;
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  
	return bytes_read;
}

int FileWrite(int handle, void *mem, int num_bytes){
	int fsize = 0;
	int vblk_start = 0;
	int vblk_end = 0;
	int bytes_write = 0;
	int i =0;
	
	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(num_bytes > 4096){return FILE_FAIL;}

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
	if(!fd[handle].inuse){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}
	if(fd[handle].mode != W && fd[handle].mode != RW){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		printf("FileWrite file mode is not write.\n");
		return FILE_FAIL;
	}

	fsize = DfsInodeFilesize(fd[handle].inode);
	if((fd[handle].curpos + num_bytes) > fsize){
		// Allocate more virtual blocks
		if((fsize % DFS_BLOCKSIZE) == 0){
			vblk_start = fsize / DFS_BLOCKSIZE;
		}
		else{
			vblk_start = fsize / DFS_BLOCKSIZE + 1;
		}
		if((fd[handle].curpos + num_bytes) % DFS_BLOCKSIZE == 0){
			vblk_end = (fd[handle].curpos + num_bytes) / DFS_BLOCKSIZE - 1; 
		}
		else{
			vblk_end = (fd[handle].curpos + num_bytes) / DFS_BLOCKSIZE;
		}
		for(i = vblk_start; i <= vblk_end; i++){
			if(DfsInodeAllocateVirtualBlock(fd[handle].inode, i) == DFS_FAIL){return DFS_FAIL;}
		}
	}
	if((bytes_write = DfsInodeWriteBytes(fd[handle].inode, mem, fd[handle].curpos, num_bytes)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
		return DFS_FAIL;
	}
	fsize = DfsInodeFilesize(fd[handle].inode);
	if(fd[handle].curpos + num_bytes < fsize){
		fd[handle].curpos += num_bytes;
	}
	else{
		fd[handle].curpos = fsize;
		fd[handle].eof = 1;
	}
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
	return bytes_write;
}

int FileSeek(int handle, int num_bytes, int from_where){
	int fsize = 0;
	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(!fd[handle].inuse){return FILE_FAIL;}

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}

	if((fsize =  DfsInodeFilesize(fd[handle].inode)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){
 			printf("FileClose couldn't release lock!\n");
 			return SYNC_FAIL;
  }
		printf("FileSeek couldn't get file size.\n");
		return FILE_FAIL;
	}

	fd[handle].eof = 0;

	switch(from_where){
		case FILE_SEEK_CUR:
			if((num_bytes + fd[handle].curpos) < 0){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				printf("FileSeek tries to set position to negative value.\n");
				return FILE_FAIL;
			}
			if((num_bytes + fd[handle].curpos) < fsize){
				fd[handle].curpos += num_bytes;
				break;
			}
			fd[handle].curpos = fsize;
			break;
		case FILE_SEEK_SET:
			if(num_bytes < 0){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				printf("FileSeek tries to set position to negative value.\n");
				return FILE_FAIL;
			}
			if(num_bytes < fsize){
				fd[handle].curpos = num_bytes;
				break;
			}
			fd[handle].curpos = fsize;
			break;
		case FILE_SEEK_END:
			if(fsize + num_bytes < 0 || num_bytes > 0){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				printf("FileSeek tries to set position to negative value or beyond the file.\n");
				return FILE_FAIL;
			}
			fd[handle].curpos =  fsize + num_bytes;
			break;
		default:
			if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
			return FILE_FAIL;
	}
	if(fd[handle].curpos >= fsize){
		fd[handle].eof = 1;
	}

	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}

	return FILE_SUCCESS;
}

int FileSetMode(int handle, char * mode){
	if((int)dstrstr(mode, "rw") != 0){
		fd[handle].mode = RW;
	}
	else if((int)dstrstr(mode, "r") != 0){
		fd[handle].mode = R;
	}
	else if((int)dstrstr(mode, "w") != 0){
		fd[handle].mode = W;
	}
	else{
		printf("FileSetMode file mode is wrong.\n");
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}
	return FILE_SUCCESS;
}


//--------------------------------------------------------------------------
//	convertModeToInt
//	
//	Converts a mode string to integer. Mode "r" is 1, mode "w" is 2. The various 
//  combinations are obtained by adding these basic modes.
//--------------------------------------------------------------------------
int convertModeToInt(char *mode)
{
  int i;
  int md = 0;

  for(i=0; mode[i]!='\0'; i++)
  {
    switch(mode[i])
    {
      case 'r':
      case 'R':
        md |= R;
	break;
      case 'w':
      case 'W':
        md |= W;
	break;
      default:
        return 0;
    }
  }
  return md;
}