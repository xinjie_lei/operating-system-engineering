#include "usertraps.h"
#include "misc.h"

#include "fdisk.h"

dfs_superblock sb;
dfs_inode inodes[FDISK_INODE_NUM_BLOCKS];
uint32 fbv[DFS_FBV_MAX_NUM_WORDS];

int diskblocksize = 0; // These are global in order to speed things up
int disksize = 0;      // (i.e. fewer traps to OS to get the same number)

int FdiskWriteBlock(uint32 blocknum, dfs_block *b); //You can use your own function. This function 
//calls disk_write_block() to write physical blocks to disk

void main (int argc, char *argv[])
{
  int i, j; // loop index
  dfs_block b; // a dfs_block holds metadata in FdiskWriteBlock
  int num_filesystem_blocks =0;
  int num_inode_blocks=0;
  int num_fbv_blocks=0;
  int bytes_write;
	// STUDENT: put your code here. Follow the guidelines below. They are just the main steps. 
	// You need to think of the finer details. You can use bzero() to zero out bytes in memory

  //Initializations and argc check
	if(argc != 1){
		Printf("Usage: %s\n");
	}

  // Need to invalidate filesystem before writing to it to make sure that the OS
  // doesn't wipe out what we do here with the old version in memory
  // You can use dfs_invalidate(); but it will be implemented in Problem 2. You can just do 
  // sb.valid = 0
  dfs_invalidate();
  disksize = disk_size();
  diskblocksize = disk_blocksize();
  num_filesystem_blocks = disksize / diskblocksize / 2;
  num_inode_blocks = FDISK_INODE_NUM_BLOCKS;
  num_fbv_blocks = num_filesystem_blocks / 8 / DFS_BLOCKSIZE;

  // Make sure the disk exists before doing anything else
  if(disk_create() == DISK_FAIL){
    Printf("FATAL ERROR: couldn't create a disk!\n");
    Exit();
  }
  // Write all inodes as not in use and empty (all zeros)
  for(i = 0; i < FDISK_NUM_INODES; i++){
    inodes[i].type = (char)0;
    inodes[i].permission = (char)0;
    inodes[i].ownerid = 0;
    inodes[i].inuse = 0;
    inodes[i].num_blocks = 0;
    inodes[i].num_entries = 0;
    inodes[i].size = 0;
    inodes[i].indirect = 0;
    for(j = 0; j < 10; j++){
      inodes[i].direct[j] = 0;
    }
  }
  for(i = 0; i < num_inode_blocks; i++){
    bcopy((char*)inodes + i * sizeof(dfs_block), b.data, sizeof(dfs_block));
    if((bytes_write = FdiskWriteBlock(i + FDISK_INODE_BLOCK_START, &b)) == DFS_FAIL){
      Printf("FdiskWriteBlock couldn't write to disk.\n");
      Exit();
    }
    bzero(b.data, sizeof(dfs_block));
  }
  // Next, setup free block vector (fbv) and write free block vector to the disk
  fbv[0] = 0x0001fffff;
  for(i = 1; i < DFS_FBV_MAX_NUM_WORDS; i++){
    fbv[i] = 0x0;
  }
  for(i = 0; i < num_fbv_blocks; i++){
    bcopy((char*)fbv + i * sizeof(dfs_block), b.data, sizeof(dfs_block));
    if((bytes_write = FdiskWriteBlock(i + FDISK_FBV_BLOCK_START, &b)) == DFS_FAIL){
      Printf("FdiskWriteBlock couldn't write to disk.\n");
      Exit();
    }
    bzero(b.data, sizeof(dfs_block)); 
  }
  // Finally, setup superblock as valid filesystem and write superblock and boot record to disk: 
  // boot record is all zeros in the first physical block, and superblock structure goes into the second physical block
  sb.blk_size = DFS_BLOCKSIZE;
  sb.blk_num = num_filesystem_blocks;
  sb.inode_blk_start = FDISK_INODE_BLOCK_START;
  sb.inode_num = FDISK_NUM_INODES;
  sb.inode_blk_num = num_inode_blocks;;
  sb.fbv_blk_start = FDISK_FBV_BLOCK_START;
  sb.fbv_blk_num = num_fbv_blocks;
  sb.data_blk_start = FDISK_FBV_BLOCK_START + num_fbv_blocks;
  sb.data_blk_num = 10 + DFS_BLOCKSIZE / 4;
  
  bcopy((char*)&sb, b.data + diskblocksize, sizeof(sb));
  if((bytes_write = FdiskWriteBlock(FDISK_BOOT_FILESYSTEM_BLOCKNUM, &b)) == DFS_FAIL){
    Printf("FdiskWriteBlock couldn't write to disk.\n");
    Exit();
  }
  Printf("fdisk (%d): Formatted DFS disk for %d bytes.\n", getpid(), disksize);
}

int FdiskWriteBlock(uint32 blocknum, dfs_block *b) {
  // STUDENT: put your code here
  int bytes_write = 0;
  int write_num = DFS_BLOCKSIZE / diskblocksize;
  int disk_blocknum = blocknum * write_num;
  int i = 0;
  for(i = 0; i < write_num; i++){
    if((bytes_write = disk_write_block(disk_blocknum + i, b->data + i * diskblocksize)) == DISK_FAIL){
      Printf("FATAL ERROR: FdiskWriteBlock couldn't write to disk, blocknum = %d.\n", disk_blocknum);
      Exit();
    }
  }
  return bytes_write;
}
