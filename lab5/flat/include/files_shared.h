#ifndef __FILES_SHARED__
#define __FILES_SHARED__

#define FILE_SEEK_SET 1
#define FILE_SEEK_END 2
#define FILE_SEEK_CUR 3

#define FILE_MAX_FILENAME_LENGTH 44
#define FILE_MAX_READWRITE_BYTES 4096
typedef struct file_descriptor {
  // STUDENT: put file descriptor info here
  int inuse;
  char filename[FILE_MAX_FILENAME_LENGTH];
  int inode;
  int curpos;
  int eof;
  int mode;
} file_descriptor;

#define FILE_FAIL -1
#define FILE_EOF -1
#define FILE_SUCCESS 1

#define	FILE_MODE_READ	0x1
#define	FILE_MODE_WRITE	0x2
#define	FILE_MODE_RW	(FILE_MODE_READ | FILE_MODE_WRITE)
#endif
