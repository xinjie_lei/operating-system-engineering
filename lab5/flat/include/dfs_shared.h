#ifndef __DFS_SHARED__
#define __DFS_SHARED__

typedef struct dfs_superblock {
  // STUDENT: put superblock internals here
	int valid;
	int blk_size;
	int blk_num;
	int inode_blk_start;
	int inode_num;
	int inode_blk_num;
	int fbv_blk_start;
	int fbv_blk_num;
  int data_blk_start;
  int data_blk_num;
} dfs_superblock;

#define DFS_BLOCKSIZE 1024  // Must be an integer multiple of the disk blocksize

typedef struct dfs_block {
  char data[DFS_BLOCKSIZE];
} dfs_block;

typedef struct dfs_inode {
  // STUDENT: put inode structure internals here
  // IMPORTANT: sizeof(dfs_inode) MUST return 128 in order to fit in enough
  // inodes in the filesystem (and to make your life easier).  To do this, 
  // adjust the maximumm length of the filename until the size of the overall inode 
  // is 128 bytes.
  int inuse;
  int size;
  char filename[44];
  uint32 direct[10];
  uint32 indirect;
} dfs_inode;

#define DFS_MAX_FILESYSTEM_SIZE 0x1000000  // 16MB
#define DFS_FBV_MAX_NUM_WORDS (DFS_MAX_FILESYSTEM_SIZE/DFS_BLOCKSIZE/32)
#define DFS_NUM_INODES 192
#define DFS_FAIL -1
#define DFS_SUCCESS 1



#endif
