#include "ostraps.h"
#include "dlxos.h"
#include "traps.h"
#include "queue.h"
#include "disk.h"
#include "dfs.h"
#include "synch.h"

static dfs_inode inodes[DFS_NUM_INODES]; // all inodes
static dfs_superblock sb; // superblock
static uint32 fbv[DFS_FBV_MAX_NUM_WORDS]; // Free block vector
static lock_t mutex;
static uint32 negativeone = 0xFFFFFFFF;
static inline uint32 invert(uint32 n) { return n ^ negativeone; }

// You have already been told about the most likely places where you should use locks. You may use 
// additional locks if it is really necessary.

// STUDENT: put your file system level functions below.
// Some skeletons are provided. You can implement additional functions.

///////////////////////////////////////////////////////////////////
// Non-inode functions first
///////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------
// DfsModuleInit is called at boot time to initialize things and
// open the file system for use.
//-----------------------------------------------------------------

void DfsModuleInit() {
// You essentially set the file system as invalid and then open 
// using DfsOpenFileSystem().
	mutex = LockCreate();
	DfsInvalidate();
	DfsOpenFileSystem();
	FileModuleInit();
}

//-----------------------------------------------------------------
// DfsInavlidate marks the current version of the filesystem in
// memory as invalid.  This is really only useful when formatting
// the disk, to prevent the current memory version from overwriting
// what you already have on the disk when the OS exits.
//-----------------------------------------------------------------

void DfsInvalidate() {
// This is just a one-line function which sets the valid bit of the 
// superblock to 0.
	sb.valid = 0;
}

//-------------------------------------------------------------------
// DfsOpenFileSystem loads the file system metadata from the disk
// into memory.  Returns DFS_SUCCESS on success, and DFS_FAIL on 
// failure.
//-------------------------------------------------------------------

int DfsOpenFileSystem() {
	int bytes_read = 0;
	int i = 0;
	disk_block b;
	dfs_block inode_blk;
	dfs_block fbv_blk;
//Basic steps:
// Check that filesystem is not already open
	if(sb.valid){
		printf("DfsOpenFileSystem tries to open an opened file system!\n");
		return DFS_FAIL;
	}

// Read superblock from disk.  Note this is using the disk read rather 
// than the DFS read function because the DFS read requires a valid 
// filesystem in memory already, and the filesystem cannot be valid 
// until we read the superblock. Also, we don't know the block size 
// until we read the superblock, either.
	if((bytes_read = DiskReadBlock(1, &b)) == DISK_FAIL){
		printf("DfsOpenFileSystem couldn't read superblock from disk to memory!\n");
		return DFS_FAIL;
	}
// Copy the data from the block we just read into the superblock in memory
	bcopy(b.data, (char*)&sb, sizeof(sb));
	sb.valid = 1;

// All other blocks are sized by virtual block size:
// Read inodes
	for(i = 0; i < sb.inode_blk_num; i++){
		if((bytes_read = DfsReadBlock(sb.inode_blk_start + i, &inode_blk)) == DFS_FAIL){
			printf("DfsOpenFileSystem coundn't read inodes from disk!\n");
			return DFS_FAIL;
		}
		bcopy(inode_blk.data, (char*)(inodes) + i * sizeof(dfs_block), bytes_read);
	}
// Read free block vector
	for(i = 0; i < sb.fbv_blk_num; i++){
		if((bytes_read = DfsReadBlock(sb.fbv_blk_start + i, &fbv_blk)) == DFS_FAIL){
			printf("DfsOpenFileSystem coundn't read free block vector from disk!\n");
			return DFS_FAIL;
		}
		bcopy(fbv_blk.data, (char*)(fbv) + i * sizeof(dfs_block), bytes_read);
	}
// Change superblock to be invalid, write back to disk, then change 
// it back to be valid in memory
	DfsInvalidate();
	bzero(b.data, sizeof(b.data));
	bcopy((char*)&sb, b.data, sizeof(sb));
	if((bytes_read = DiskWriteBlock(1, &b)) == DISK_FAIL){
		printf("DfsOpenFileSystem coundn't write superblock to disk!\n");
		return DFS_FAIL;
	}
	sb.valid = 1;
	return DFS_SUCCESS;
}


//-------------------------------------------------------------------
// DfsCloseFileSystem writes the current memory version of the
// filesystem metadata to the disk, and invalidates the memory's 
// version.
//-------------------------------------------------------------------

int DfsCloseFileSystem() {
	int bytes_write = 0;
	int i = 0;
	disk_block b;
	dfs_block inode_blk;
	dfs_block fbv_blk;

	if(!sb.valid){
		printf("DfsCloseFileSystem tries to close an closed file system!\n");
		return DFS_FAIL;
	}
// Write inodes
	for(i = 0; i < sb.inode_blk_num; i++){
		bcopy((char *)(inodes) + i * sizeof(dfs_block), inode_blk.data, sizeof(dfs_block));
		if((bytes_write = DfsWriteBlock(sb.inode_blk_start + i, &inode_blk)) == DFS_FAIL){
			printf("DfsCloseFileSystem coundn't write inodes to disk!\n");
			return DFS_FAIL;
		}
	}

// Write free block vector
	for(i = 0; i < sb.fbv_blk_num; i++){
		bcopy((char *)(fbv) + i * sizeof(dfs_block), fbv_blk.data, sizeof(dfs_block));
		if((bytes_write = DfsWriteBlock(sb.fbv_blk_start + i, &fbv_blk)) == DFS_FAIL){
			printf("DfsOpenFileSystem coundn't write free block vector to disk!\n");
			return DFS_FAIL;
		}
	}

	bcopy((char*)&sb, b.data, sizeof(sb));
	if((bytes_write = DiskWriteBlock(1, &b)) == DISK_FAIL){
		printf("DfsOpenFileSystem coundn't write superblock to disk!\n");
		return DFS_FAIL;
	}
	return DFS_SUCCESS;
}


//-----------------------------------------------------------------
// DfsAllocateBlock allocates a DFS block for use. Remember to use 
// locks where necessary.
//-----------------------------------------------------------------

uint32 DfsAllocateBlock() {
// Check that file system has been validly loaded into memory
// Find the first free block using the free block vector (FBV), mark it in use
// Return handle to block

	static int mapnum = 0;
	int bitnum = 0;

	if(!sb.valid){return DFS_FAIL;}

	dbprintf ('f', "Allocating block, starting with block %d\n", mapnum);
	while (fbv[mapnum] == 0xffffffff) {
		mapnum += 1;
		if (mapnum >= DFS_FBV_MAX_NUM_WORDS) {
			break;
		}
	}
	if(mapnum >= DFS_FBV_MAX_NUM_WORDS){return DFS_FAIL;}
	for (bitnum = 0; (fbv[mapnum] & (1 << bitnum)) == (1 << bitnum); bitnum++) {
	}
	fbv[mapnum] |= 1 << bitnum;
	dbprintf ('f', "Allocated block, from map %d, block =%d, fbv=0x%x.\n", mapnum, (mapnum * 32) + bitnum, fbv[mapnum]);
	return ((mapnum * 32) + bitnum);
}

uint32 DfsAllocateVirtualBlock(uint32 handle, uint32 virtual_blocknum){
	dfs_block b;
	uint32 indirect_table[sb.blk_size/sizeof(int)];
	uint32 fbn;

	if(virtual_blocknum < 10){
		if((fbn=DfsAllocateBlock()) == DFS_FAIL){return DFS_FAIL;}
		inodes[handle].direct[virtual_blocknum] = fbn;
		return fbn;
	}

	if(inodes[handle].indirect == 0){
		if((fbn=DfsAllocateBlock()) == DFS_FAIL){return DFS_FAIL;}
		inodes[handle].indirect = fbn;
		bzero((char*)indirect_table, sb.blk_size);
		if((fbn=DfsAllocateBlock()) == DFS_FAIL){return DFS_FAIL;}
		indirect_table[virtual_blocknum-10] = fbn;
		bcopy((char*)indirect_table, b.data, sb.blk_size);
		if(DfsWriteBlock(inodes[handle].indirect, &b) == DFS_FAIL){return DFS_FAIL;}
	}
	else{
		if(DfsReadBlock(inodes[handle].indirect, &b) == DFS_FAIL){return DFS_FAIL;}
		bcopy(b.data, (char*)indirect_table, sb.blk_size);
		if((fbn=DfsAllocateBlock()) == DFS_FAIL){return DFS_FAIL;}
		indirect_table[virtual_blocknum - 10] = fbn;
		bcopy((char*)indirect_table, b.data, sb.blk_size);
		if(DfsWriteBlock(inodes[handle].indirect, &b) == DFS_FAIL){return DFS_FAIL;}
	}
	return fbn;
}

//-----------------------------------------------------------------
// DfsFreeBlock deallocates a DFS block.
//-----------------------------------------------------------------

int DfsFreeBlock(uint32 blocknum) {
	if(!sb.valid){return DFS_FAIL;}
	if(blocknum < sb.data_blk_start){return DFS_FAIL;}
	fbv[blocknum / 32] &= invert(1 << (blocknum % 32));
	return DFS_SUCCESS;
}

int DfsFreeAllDataBlocks(uint32 handle){
	int i = 0; // loop index
	int indirect_num = sb.blk_size/4; // uint32 = 4bytes
	dfs_block b;

	if(inodes[handle].indirect != 0){
		if(DfsReadBlock(inodes[handle].indirect, &b) == DFS_FAIL){
			return DFS_FAIL;
		}
		for(i = 0; i < indirect_num; i++){
			if((uint32)b.data[i] != 0){
				if(DfsFreeBlock((uint32)b.data[i]) == DFS_FAIL){
					return DFS_FAIL;
				}
			}
		}
		if(DfsFreeBlock(inodes[handle].indirect) == DFS_FAIL){
			return DFS_FAIL;
		}
	}
	for(i = 0; i < 10; i++){
		if(inodes[handle].direct[i] != 0){
			if(DfsFreeBlock(inodes[handle].direct[i]) == DFS_FAIL){
				return DFS_FAIL;
			}
			inodes[handle].direct[i] = 0;
		}
	}
	inodes[handle].size = 0;
	return DFS_SUCCESS;
}
//-----------------------------------------------------------------
// DfsReadBlock reads an allocated DFS block from the disk
// (which could span multiple physical disk blocks).  The block
// must be allocated in order to read from it.  Returns DFS_FAIL
// on failure, and the number of bytes read on success.  
//-----------------------------------------------------------------

int DfsReadBlock(uint32 blocknum, dfs_block *b) {
	disk_block db;
	int bytes_read = 0;
	int read_num = sb.blk_size / DISK_BLOCKSIZE;
	int disk_blocknum = blocknum * read_num;
	int i = 0;

	if(!sb.valid){return DFS_FAIL;}

	for(i = 0; i < read_num; i++){
		if((bytes_read = DiskReadBlock(disk_blocknum + i, &db)) == DISK_FAIL){
			printf("FATAL ERROR: DfsReadBlock coundn't read from disk\n");
			GracefulExit();
		}
		bcopy(db.data, b->data + DISK_BLOCKSIZE * i, DISK_BLOCKSIZE);
		bzero(db.data, bytes_read);
	}
	return bytes_read * read_num;
}


//-----------------------------------------------------------------
// DfsWriteBlock writes to an allocated DFS block on the disk
// (which could span multiple physical disk blocks).  The block
// must be allocated in order to write to it.  Returns DFS_FAIL
// on failure, and the number of bytes written on success.  
//-----------------------------------------------------------------

int DfsWriteBlock(uint32 blocknum, dfs_block *b){
	disk_block db;
	int write_num = sb.blk_size / DISK_BLOCKSIZE;
	int disk_blocknum = blocknum * write_num;
	int bytes_write = 0;
	int i = 0;

	if(!sb.valid){return DFS_FAIL;}
	for(i = 0; i < write_num; i++){
		bcopy(b->data + i * DISK_BLOCKSIZE, db.data, DISK_BLOCKSIZE);
		if((bytes_write = DiskWriteBlock(disk_blocknum + i, &db)) == DISK_FAIL){
			printf("FATAL ERROR: DfsReadBlock coundn't read from disk\n");
			GracefulExit();
		}
		bzero(db.data, bytes_write);
	}
	return bytes_write * write_num;
}


////////////////////////////////////////////////////////////////////////////////
// Inode-based functions
////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------
// DfsInodeFilenameExists looks through all the inuse inodes for 
// the given filename. If the filename is found, return the handle 
// of the inode. If it is not found, return DFS_FAIL.
//-----------------------------------------------------------------

uint32 DfsInodeFilenameExists(char *filename) {
	int i = 0; // loop index
	if(filename == NULL){return DFS_FAIL;}
	for(i = 0; i < sb.inode_num; i++){
		if(inodes[i].inuse && dstrncmp(inodes[i].filename, filename, sizeof(inodes[i].filename)) == 0){
			break;
		}
	} 
	if(i == sb.inode_num){return DFS_FAIL;}
	return i;
}


//-----------------------------------------------------------------
// DfsInodeOpen: search the list of all inuse inodes for the 
// specified filename. If the filename exists, return the handle 
// of the inode. If it does not, allocate a new inode for this 
// filename and return its handle. Return DFS_FAIL on failure. 
// Remember to use locks whenever you allocate a new inode.
//-----------------------------------------------------------------

uint32 DfsInodeOpen(char *filename) {
	int i = 0; // loop index
	int handle = 0;
	int j = 0;

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return DFS_FAIL;}

	if((handle = DfsInodeFilenameExists(filename)) != DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return handle;
	}

	for(i = 0; i < sb.inode_num; i++){
		if(!inodes[i].inuse){break;}
	}
	if(i == sb.inode_num){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		printf("DfsInodeOpen no free inode.\n");
		return DFS_FAIL;
	}
	inodes[i].inuse = 1;
	dstrcpy(inodes[i].filename, filename);
	inodes[i].size = 0;
	inodes[i].indirect = 0;
	for(j = 0; j < 10; j++){
		inodes[i].direct[j] = 0;
	}
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
	return i;
}

int DfsInodeFreeAllDataBlocks(uint32 handle){

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return DFS_FAIL;}
	if(DfsFreeAllDataBlocks(handle) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return DFS_FAIL;
	}
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
	return DFS_SUCCESS;
}

//-----------------------------------------------------------------
// DfsInodeDelete de-allocates any data blocks used by this inode, 
// including the indirect addressing block if necessary, then mark 
// the inode as no longer in use. Use locks when modifying the 
// "inuse" flag in an inode.Return DFS_FAIL on failure, and 
// DFS_SUCCESS on success.
//-----------------------------------------------------------------

int DfsInodeDelete(uint32 handle) {
	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return DFS_FAIL;}
	if(DfsFreeAllDataBlocks(handle) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return DFS_FAIL;
	}
	inodes[handle].indirect = 0;
	inodes[handle].size = 0;
	bzero(inodes[handle].filename, sizeof(inodes[handle].filename));
	inodes[handle].inuse = 0;
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
	return DFS_SUCCESS;
}


//-----------------------------------------------------------------
// DfsInodeReadBytes reads num_bytes from the file represented by 
// the inode handle, starting at virtual byte start_byte, copying 
// the data to the address pointed to by mem. Return DFS_FAIL on 
// failure, and the number of bytes read on success.
//-----------------------------------------------------------------

int DfsInodeReadBytes(uint32 handle, void *mem, int start_byte, int num_bytes) {
	uint32 start_vbn = start_byte/sb.blk_size;
	uint32 end_vbn = 0;
	int i = 0;
	int bytes_read = 0;
	uint32 fbn;
	dfs_block b;

	if(!sb.valid){return DFS_FAIL;}
	if(start_byte < 0){return DFS_FAIL;}

	if((start_byte+num_bytes) % sb.blk_size == 0){
		end_vbn = (start_byte+num_bytes) / sb.blk_size - 1;
	}
	else{
		end_vbn = (start_byte+num_bytes) / sb.blk_size;
	}
	end_vbn = end_vbn > sb.data_blk_num ? sb.data_blk_num : end_vbn;

	for(i = start_vbn; i <= end_vbn; i++){
		if((fbn = DfsInodeTranslateVirtualToFilesys(handle, i)) == DFS_FAIL){return DFS_FAIL;}
		if(DfsReadBlock(fbn, &b) == DFS_FAIL){return DFS_FAIL;}
		if(i == start_vbn){
			if(start_vbn == end_vbn){
				bcopy(b.data + start_byte % sb.blk_size, (char*)mem, num_bytes);
				bytes_read += num_bytes;
			}
			else{
				bcopy(b.data + start_byte % sb.blk_size, (char*)mem, sb.blk_size - start_byte % sb.blk_size);
				bytes_read += sb.blk_size - start_byte % sb.blk_size;
			}
		}
		else if(i == end_vbn){
			bcopy(b.data, (char*)(mem + bytes_read), num_bytes - bytes_read);
			bytes_read += num_bytes - bytes_read;
		}
		else{
			bcopy(b.data, (char*)(mem + bytes_read), sb.blk_size);
			bytes_read += sb.blk_size;
		}
	}
	
	return bytes_read;
}


//-----------------------------------------------------------------
// DfsInodeWriteBytes writes num_bytes from the memory pointed to 
// by mem to the file represented by the inode handle, starting at 
// virtual byte start_byte. Note that if you are only writing part 
// of a given file system block, you'll need to read that block 
// from the disk first. Return DFS_FAIL on failure and the number 
// of bytes written on success.
//-----------------------------------------------------------------

int DfsInodeWriteBytes(uint32 handle, void *mem, int start_byte, int num_bytes) {
	uint32 start_vbn = start_byte/sb.blk_size;
	uint32 end_vbn = 0;
	int i = 0;
	int bytes_write = 0;
	uint32 fbn;
	dfs_block b;

	if(!sb.valid){return DFS_FAIL;}
	if(start_byte < 0){return DFS_FAIL;}

	if((start_byte+num_bytes) % sb.blk_size == 0){
		end_vbn = (start_byte+num_bytes) / sb.blk_size - 1;
	}
	else{
		end_vbn = (start_byte+num_bytes) / sb.blk_size;
	}
	end_vbn = end_vbn > sb.data_blk_num ? sb.data_blk_num : end_vbn;
	for(i = start_vbn; i <= end_vbn; i++){
		if((fbn = DfsInodeTranslateVirtualToFilesys(handle, i)) == DFS_FAIL){return DFS_FAIL;}
		if(DfsReadBlock(fbn, &b) == DFS_FAIL){return DFS_FAIL;}
		if(i == start_vbn){
			if(start_vbn == end_vbn){
				bcopy((char*)mem, b.data + start_byte % sb.blk_size, num_bytes);
				bytes_write += num_bytes;
			}
			else{
				bcopy((char*)mem, b.data + start_byte % sb.blk_size, sb.blk_size - start_byte % sb.blk_size);
				bytes_write += sb.blk_size - start_byte % sb.blk_size;
			}
		}
		else if(i == end_vbn){
			bcopy((char*)mem + bytes_write, b.data, num_bytes - bytes_write);
			bytes_write += num_bytes - bytes_write;
		}
		else{
			bcopy((char*)mem + bytes_write, b.data, sb.blk_size);
			bytes_write += sb.blk_size;
		}
		if(DfsWriteBlock(fbn, &b) == DFS_FAIL){return DFS_FAIL;}
	}
	if(inodes[handle].size < (start_byte + num_bytes)){
		inodes[handle].size = start_byte + num_bytes;
	}
	return bytes_write;

}


//-----------------------------------------------------------------
// DfsInodeFilesize simply returns the size of an inode's file. 
// This is defined as the maximum virtual byte number that has 
// been written to the inode thus far. Return DFS_FAIL on failure.
//-----------------------------------------------------------------

uint32 DfsInodeFilesize(uint32 handle) {
	if(!sb.valid){return DFS_FAIL;}
	if(handle < 0){return DFS_FAIL;}
	if(handle > DFS_NUM_INODES){return DFS_FAIL;}
	return inodes[handle].size;
}


//-----------------------------------------------------------------
// DfsInodeAllocateVirtualBlock allocates a new filesystem block 
// for the given inode, storing its blocknumber at index 
// virtual_blocknumber in the translation table. If the 
// virtual_blocknumber resides in the indirect address space, and 
// there is not an allocated indirect addressing table, allocate it. 
// Return DFS_FAIL on failure, and the newly allocated file system 
// block number on success.
//-----------------------------------------------------------------

uint32 DfsInodeAllocateVirtualBlock(uint32 handle, uint32 virtual_blocknum) {
	uint32 fbn;
	if(!sb.valid){return DFS_FAIL;}
	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
	if((fbn = DfsAllocateVirtualBlock(handle, virtual_blocknum)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return DFS_FAIL;
	}
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
	return fbn;
}



//-----------------------------------------------------------------
// DfsInodeTranslateVirtualToFilesys translates the 
// virtual_blocknum to the corresponding file system block using 
// the inode identified by handle. Return DFS_FAIL on failure.
//-----------------------------------------------------------------

uint32 DfsInodeTranslateVirtualToFilesys(uint32 handle, uint32 virtual_blocknum) {
	dfs_block b;
	uint32 indirect_table[sb.blk_size/4];

	if(!sb.valid){return DFS_FAIL;}
	if(virtual_blocknum < 10){
		return inodes[handle].direct[virtual_blocknum];
	}
	if(DfsReadBlock(inodes[handle].indirect, &b) == DISK_FAIL){return DFS_FAIL;}
	bcopy(b.data, (char*)indirect_table, sb.blk_size);
	return indirect_table[virtual_blocknum - 10];
}
