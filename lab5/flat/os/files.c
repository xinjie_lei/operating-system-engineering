#include "ostraps.h"
#include "dlxos.h"
#include "process.h"
#include "dfs.h"
#include "files.h"
#include "synch.h"

// You have already been told about the most likely places where you should use locks. You may use 
// additional locks if it is really necessary.
static file_descriptor fd[FILE_MAX_OPEN_FILES];
static lock_t mutex;

// STUDENT: put your file-level functions here
void FileModuleInit(){
	int i = 0;
	for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
		fd[i].inuse = 0;
		fd[i].inode = 0;
		fd[i].curpos = 0;
		fd[i].eof = 0;
		fd[i].mode = 0;
		bzero(fd[i].filename, sizeof(fd[i].filename));
	}
	mutex = LockCreate();
}

int FileOpen(char *filename, char *mode){
	int i = 0;
		
	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}

	for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
		if(fd[i].inuse && (dstrncmp(filename, fd[i].filename, FILE_MAX_FILENAME_LENGTH) == 0)){
					if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
					return FILE_FAIL;
		}
	}

	for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
		if(!fd[i].inuse){
			break;
		}
	}
	if(i == FILE_MAX_OPEN_FILES){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
		return FILE_FAIL;
	}
	if(FileSetMode(i, mode) == FILE_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
		return FILE_FAIL;
	}
	if((fd[i].inode = DfsInodeFilenameExists(filename)) == DFS_FAIL){
		if((fd[i].inode = DfsInodeOpen(filename)) == DFS_FAIL){
			if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
			return FILE_FAIL;	
		}
	}
	if(fd[i].mode == FILE_MODE_WRITE || fd[i].mode == FILE_MODE_RW){
		if(DfsInodeFreeAllDataBlocks(fd[i].inode) == DFS_FAIL){
			if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
			return DFS_FAIL;
		}
	}
	dstrcpy(fd[i].filename, filename);
	fd[i].inuse = 1;
	fd[i].curpos = 0;
	fd[i].eof = 0;
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
	return i;
}

int FileSetMode(int handle, char * mode){
	if((int)dstrstr(mode, "rw") != 0){
		fd[handle].mode = FILE_MODE_RW;
	}
	else if((int)dstrstr(mode, "r") != 0){
		fd[handle].mode = FILE_MODE_READ;
	}
	else if((int)dstrstr(mode, "w") != 0){
		fd[handle].mode = FILE_MODE_WRITE;
	}
	else{
		printf("FileSetMode file mode is wrong.\n");
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}
	return FILE_SUCCESS;
}

int FileClose(int handle){
	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(!fd[handle].inuse){return FILE_FAIL;}
	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}

  fd[handle].inuse = 0;
  fd[handle].curpos = 0;
  fd[handle].mode = 0;
  fd[handle].eof = 0;
  bzero(fd[handle].filename, sizeof(fd[handle].filename));
  fd[handle].inode = 0;
  if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  return FILE_SUCCESS;
}

int FileDelete(char *filename){
	int i = 0;
	int handle = 0;
	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
  for(i = 0; i < FILE_MAX_OPEN_FILES; i++){
  	if(dstrncmp(filename, fd[i].filename, FILE_MAX_FILENAME_LENGTH) == 0){
  		if(fd[i].inuse){
  			if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  			return FILE_FAIL;
  		}
  	}
  }

  if((handle = DfsInodeFilenameExists(filename)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  	return FILE_FAIL;
  }
  if(DfsInodeDelete(handle) == DFS_FAIL){
  	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  	return DFS_FAIL;
  }
  if(LockHandleRelease(mutex) != SYNC_SUCCESS){return DFS_FAIL;}
  return FILE_SUCCESS;
}


int FileRead(int handle, void *mem, int num_bytes){
	int fsize = 0;
	int bytes_read = 0;

	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(num_bytes > 4096){return FILE_FAIL;}

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
	if(!fd[handle].inuse){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}

	if(fd[handle].mode != FILE_MODE_READ && fd[handle].mode != FILE_MODE_RW){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		printf("FileRead file mode is not read.\n");
		return FILE_FAIL;
	}

	if(fd[handle].eof){
  	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  	printf("FileRead reaches end of file.\n");
  	return FILE_FAIL;
  }
	fsize = DfsInodeFilesize(fd[handle].inode);
	if((fd[handle].curpos + num_bytes) > fsize){
		num_bytes = fsize - fd[handle].curpos;
		fd[handle].eof = 1;
	}
	if((fd[handle].curpos + num_bytes) == fsize){
		fd[handle].eof = 1;
	}
	if((bytes_read = DfsInodeReadBytes(fd[handle].inode, mem, fd[handle].curpos, num_bytes)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}

	fd[handle].curpos += bytes_read;
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
  
	return bytes_read;
}

int FileWrite(int handle, void *mem, int num_bytes){
	int fsize = 0;
	int vblk_start = 0;
	int vblk_end = 0;
	int bytes_write = 0;
	int i =0;
	
	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(num_bytes > 4096){return FILE_FAIL;}

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
	if(!fd[handle].inuse){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		return FILE_FAIL;
	}
	if(fd[handle].mode != FILE_MODE_WRITE && fd[handle].mode != FILE_MODE_RW){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
		printf("FileWrite file mode is not write.\n");
		return FILE_FAIL;
	}

	fsize = DfsInodeFilesize(fd[handle].inode);
	if((fd[handle].curpos + num_bytes) > fsize){
		// Allocate more virtual blocks
		if((fsize % DFS_BLOCKSIZE) == 0){
			vblk_start = fsize / DFS_BLOCKSIZE;
		}
		else{
			vblk_start = fsize / DFS_BLOCKSIZE + 1;
		}
		if((fd[handle].curpos + num_bytes) % DFS_BLOCKSIZE == 0){
			vblk_end = (fd[handle].curpos + num_bytes) / DFS_BLOCKSIZE - 1; 
		}
		else{
			vblk_end = (fd[handle].curpos + num_bytes) / DFS_BLOCKSIZE;
		}
		for(i = vblk_start; i <= vblk_end; i++){
			if(DfsInodeAllocateVirtualBlock(fd[handle].inode, i) == DFS_FAIL){return DFS_FAIL;}
		}
	}
	if((bytes_write = DfsInodeWriteBytes(fd[handle].inode, mem, fd[handle].curpos, num_bytes)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}
		return DFS_FAIL;
	}
	fsize = DfsInodeFilesize(fd[handle].inode);
	if(fd[handle].curpos + num_bytes < fsize){
		fd[handle].curpos += num_bytes;
	}
	else{
		fd[handle].curpos = fsize;
		fd[handle].eof = 1;
	}
	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
	return bytes_write;
}

int FileSeek(int handle, int num_bytes, int from_where){
	int fsize = 0;
	if(handle < 0 || handle > FILE_MAX_OPEN_FILES){return FILE_FAIL;}
	if(!fd[handle].inuse){return FILE_FAIL;}

	if(LockHandleAcquire(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}

	if((fsize =  DfsInodeFilesize(fd[handle].inode)) == DFS_FAIL){
		if(LockHandleRelease(mutex) != SYNC_SUCCESS){
 			printf("FileClose couldn't release lock!\n");
 			return SYNC_FAIL;
  }
		printf("FileSeek couldn't get file size.\n");
		return FILE_FAIL;
	}

	fd[handle].eof = 0;

	switch(from_where){
		case FILE_SEEK_CUR:
			if((num_bytes + fd[handle].curpos) < 0){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				printf("FileSeek tries to set position to negative value.\n");
				return FILE_FAIL;
			}
			if((num_bytes + fd[handle].curpos) < fsize){
				fd[handle].curpos += num_bytes;
				break;
			}
			fd[handle].curpos = fsize;
			break;
		case FILE_SEEK_SET:
			if(num_bytes < 0){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				printf("FileSeek tries to set position to negative value.\n");
				return FILE_FAIL;
			}
			if(num_bytes < fsize){
				fd[handle].curpos = num_bytes;
				break;
			}
			fd[handle].curpos = fsize;
			break;
		case FILE_SEEK_END:
			if(fsize + num_bytes < 0 || num_bytes > 0){
				if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
				printf("FileSeek tries to set position to negative value or beyond the file.\n");
				return FILE_FAIL;
			}
			fd[handle].curpos =  fsize + num_bytes;
			break;
		default:
			if(LockHandleRelease(mutex) != SYNC_SUCCESS){return SYNC_FAIL;}
			return FILE_FAIL;
	}
	if(fd[handle].curpos >= fsize){
		fd[handle].eof = 1;
	}

	if(LockHandleRelease(mutex) != SYNC_SUCCESS){return FILE_FAIL;}

	return FILE_SUCCESS;
}
