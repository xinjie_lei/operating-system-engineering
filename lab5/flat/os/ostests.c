#include "ostraps.h"
#include "dlxos.h"
#include "traps.h"
#include "disk.h"
#include "dfs.h"

void RunOSTests() {
  // STUDENT: run any os-level tests here
  int i = 0;
  char filename_short[9] = "Lesson 1";
  char filename_long[9] = "Lesson 2";
  char filename_verylong[9] = "Lesson 3";
  char filename_passage[9] = "Lesson 4";
  char story_short[524] = "Last week I went to the theatre. I had a very good seat. The play was very interesting. I did not enjoy it. A young man and a young woman were sitting behind me. They were talking loudly. I got very angry. I could not hear the actors. I turned round. I looked at the man and the woman angrily. They did not pay any attention. In the end, I could not bear it. I turned round again. ‘I can't hear a word!’ I said angrily.‘It's none of your business, ’ the young man said rudely. ‘This is a private conversation!’\n";
 	char tale_short[524] = "";
 	char story_long[1511] = "Pumas are large, cat-like animals which are found in America. When reports came into London Zoo that a wild puma had been spotted forty-five miles south of London, they were not taken seriously. However, as the evidence began to accumulate, experts from the Zoo felt obliged to investigate, for the descriptions given by people who claimed to have seen the puma were extraordinarily similar. The hunt for the puma began in a small village where a woman picking blackberries saw‘a large cat’only five yards away from her. It immediately ran away when she saw it, and experts confirmed that a puma will not attack a human being unless it is cornered. The search proved difficult, for the puma was often observed at one place in the morning and at another place twenty miles away in the evening. Wherever it went, it left behind it a trail of dead deer and small animals like rabbits. Paw prints were seen in a number of places and puma fur was found clinging to bushes. Several people complained of‘cat-like noises’at night and a businessman on a fishing trip saw the puma up a tree. The experts were now fully convinced that the animal was a puma, but where had it come from? As no pumas had been reported missing from any zoo in the country, this one must have been in the possession of a private collector and somehow managed to escape. The hunt went on for several weeks, but the puma was not caught. It is disturbing to think that a dangerous wild animal is still at large in the quiet countryside.\n";
 	char tale_long[1511] = "";
 	char story_verylong[2263] = "There is no shortage of tipsters around offering ‘get-rich-quick’ opportunities. But if you are a serious private investor, leave the Las Vegas mentality to those with money to fritter. The serious investor needs a proper ‘portfolio’ – a well-planned selection of investments, with a definite structure and a clear aim. But exactly how does a newcomer to the stock market go about achieving that? Well, if you go to five reputable stock brokers and ask them what you should do with your money, you’re likely to get five different answers, -- even if you give all the relevant information about your age, family, finances and what you want from your investments. Moral? There is no one ‘right’ way to structure a portfolio. However, there are undoubtedly some wrong ways, and you can be sure that none of our five advisers would have suggested sinking all (or perhaps any ) of your money into Periwigs. So what should you do? We’ll assume that you have sorted out the basics – like mortgages, pensions, insurance and access to sufficient cash reserves. You should then establish your own individual aims. These are partly a matter of personal circumstances, partly a matter of psychology. For instance, if you are older you have less time to recover from any major losses, and you may well wish to boost your pension income. So preserving your capital and generating extra income are your main priorities. In this case, you’d probably construct a portfolio with some shares(but not high risk ones), along with gifts, cash deposits, and perhaps convertibles or the income shares of split capital investment trusts. If you are younger, and in a solid financial position, you may decide to take an aggressive approach – but only if you ‘re blessed with a sanguine disposition and won’t suffer sleepless nights over share prices. If you recognize yourself in this description, you might include a couple of heady growth stocks in your portfolio, alongside your more pedestrian investments. Once you have decides on your investment aims, you can then decide where to put your money. The golden rule here is spread your risk – if you put all of your money into Periwigs International, you’re setting yourself up as a hostage to fortune. ";
	char tale_verylong[2263] = "";
	char passage[12101] = "Mr. Trump, since you announced your candidacy for president, teaching high school social studies has become more interesting. Other than comedians, students have gained more from your White House run than anyone.As a high school American government teacher, I recognize that you are in a great position to win the nomination. The U.S. Constitution and the values it embodies serve as the foundation of our curriculum, so when my students ask questions about the election, the role of the president and you, their interest brings more meaning to our discourse. Mr. Trump, taking questions about you has been a challenge. The cornerstone of your immigration policy includes building a “great wall” and having Mexico pay for it, so naturally students have questions. When you said, “Look at that face! Would anyone vote for that?” about opponent Carly Fiorina, students wondered about your character. As you talked about “bombing the (expletive)” out of enemies and killing their families or banning all Muslims from entering the country, we took notice in class. Mr. Trump, you have put me in a precarious position as a professional educator. My ability to remain neutral, as you brazenly assault the American values I have spent my career promoting, became more difficult every time you found a microphone. Referring to African Americans as, “the blacks” and bragging publicly about having a young “beautiful piece of (expletive)” are just a couple of the red flags my students have raised in class.Mr. Trump, behavior like yours in a classroom would stifle learning and make parents cringe. You actually referenced your penis size in a national debate and publicly denigrated the appearance of a political rival’s wife. Your preposterous comments and arrogance justify students’ concerns about your temperament. For many of my students, who study history and defend the Bill of Rights, it is inconceivable for them to support you.At one time, we laughed at your antics. We had never heard a political figure say the things you were saying and in a manner that defied even modest political calculations. As you sunk to new lows, students became victims of the circus atmosphere you were creating.Mr. Trump, not taking you seriously was a mistake I deeply regret — as a teacher and as a citizen. Your candidacy has ushered in a sobering realization that more Americans are impaired by prejudice and anger than my students thought was possible. Mr. Trump, you have reached millions of people with a disturbing political message and nasty tactics. Your behavior is repulsive, but professional integrity requires that I attempt to understand your appeal. Mistakenly, I expected that decency would always have a role in any legitimate campaign for president. As a veteran teacher, I failed to recognize the consequences of the depraved manner in which you pedal fear to hungry crowds. We never saw your rise coming. Once the laughter died down you were leading the Republican field by huge margins, and the teacher became the student. Your lively political rallies with threats of violence and lessons about greatness woke me up. The punchlines became storylines that paint a picture of a hopeless, deeply confused nation. You are enthusiastically leading an insurgency against goodwill that my students will spend a lifetime undoing. Yet, I remain optimistic about the future of our nation in the face of your existence. Mr. Trump, I am hopeful because my students are gaining a deeper appreciation of American values like promoting the common good and fighting for equality. Students are embracing diversity and individual rights as those values come to life on the campaign trail. Student attitudes are being shaped, in part, by opposition to your toxic rhetoric. As many students prepare to vote, they recognize the threat your presidency would pose to the world.Mr. Trump, you have put me in a precarious position as a professional educator. My ability to remain neutral, as you brazenly assault the American values I have spent my career promoting, became more difficult every time you found a microphone. Referring to African Americans as, ?the blacks? and bragging publicly about having a young ?beautiful piece of (expletive)? are just a couple of the red flags my students have raised in class. Mr. Trump, behavior like yours in a classroom would stifle learning and make parents cringe. You actually referenced your penis size in a national debate and publicly denigrated the appearance of a political rival?s wife. Your preposterous comments and arrogance justify students? concerns about your temperament. For many of my students, who study history and defend the Bill of Rights, it is inconceivable for them to support you. At one time, we laughed at your antics. We had never heard a political figure say the things you were saying and in a manner that defied even modest political calculations. As you sunk to new lows, students became victims of the circus atmosphere you were creating. Mr. Trump, not taking you seriously was a mistake I deeply regret ? as a teacher and as a citizen. Your candidacy has ushered in a sobering realization that more Americans are impaired by prejudice and anger than my students thought was possible. Mr. Trump, you have reached millions of people with a disturbing political message and nasty tactics. Your behavior is repulsive, but professional integrity requires that I attempt to understand your appeal. Mistakenly, I expected that decency would always have a role in any legitimate campaign for president. As a veteran teacher, I failed to recognize the consequences of the depraved manner in which you pedal fear to hungry crowds. We never saw your rise coming. Once the laughter died down you were leading the Republican field by huge margins, and the teacher became the student. Your lively political rallies with threats of violence and lessons about greatness woke me up. The punchlines became storylines that paint a picture of a hopeless, deeply confused nation. You are enthusiastically leading an insurgency against goodwill that my students will spend a lifetime undoing. Yet, I remain optimistic about the future of our nation in the face of your existence. Mr. Trump, I am hopeful because my students are gaining a deeper appreciation of American values like promoting the common good and fighting for equality. Students are embracing diversity and individual rights as those values come to life on the campaign trail. Student attitudes are being shaped, in part, by opposition to your toxic rhetoric. As many students prepare to vote, they recognize the threat your presidency would pose to the world. Mr. Trump, at worst your popularity is a cruel reminder that many of our neighbors and friends have xenophobic leanings and a worldview skewed by sexism and bigotry. At best, your entertainment value has clouded the judgment of too many voters. Regardless, for my curious students, you are their first living lesson on the perils of populist racism. Hatred and bigotry, really bad ideas before you came along, are now alive for first-hand analysis in every classroom coast to coast. You are energizing a generation of young people to fight back. Once you flame out, they won’t let you happen again. This is the source of my optimism. Instead of teasing modern angles out of the lessons from Jim Crow America, we have you: a national political figure exposing the ugliness that occurs when power and bigotry mix in the absence of humility. In social studies classrooms, divergent points of view and civil dialogue co-exist. Your absence on the national stage will encourage informed conversation that will advance progress. The legacy of your successful run will be overshadowed by the lessons we learned from your disgraceful campaign. Lessons that will live much longer than the harm you inflict today. Today, you are trending. Tomorrow, your story will be the one about the damage that is unleashed when fear controls the political narrative. The lesson about how slowly the electorate digested your hostile message about American greatness will stay with us a long time, Mr. Trump. Your candidacy will be a lesson about temptation and averting disaster. Mr. Trump, you are a reminder that progress is not dependent on a specific political party or the ambitions of one man. Advancing American democracy demands a citizenry that is vigilant and informed. You, Mr. Trump, are the pathetic reminder that we needed a pathetic reminder. Nick Gregory, High School Social Studies Teacher. A teenager, waiting years for trial in solitary confinement. An officer, physically maimed by the inmates he works each day to protect. For years, these stories of abuse and negligence have emerged from Rikers Island all too frequently. Today, as we continue to drive down crime, the idea of closing Rikers has returned to the forefront. Yet while this idea deserves serious consideration, we cannot allow the closure conversation to distract from jail reform needed now long before any possible transition from Rikers could become reality. And we must make sure that in calls for Rikers? closure, our city does not become more focused on shutting down the facility than ending the culture that gave rise to its infamy. We must focus on strategies to reduce violence, use the tools at our disposal to reduce recidivism, and safely decrease the jail population and we must do this now, no matter where we house our jails in the future. These are the reforms I?m focused on as Mayor. We're training our staff to carefully deescalate situations, work with young inmates, and help those with mental illness resulting in an 11 percent drop in serious assaults on staff and a 23 percent drop in serious use-of-force incidents last year. We?ve reduced the use of punitive segregation by over two-thirds. We?ve opened specialized housing units so that those with mental illness receive the care they need. And in the new Beacon units where we?ve concentrated Commissioner Ponte?s reforms from more jobs training to higher staffing ratios to steady officer posts. We've had zero stabbings or slashings in more than six months. We're also working to safely decrease the jail population by reducing unnecessary arrests, adding supervised-release slots, and speeding trials up. In partnership with courts, prosecutors, defenders and mayoral agencies, we are working to reduce case delays ? and have already cleared 75 percent of the targeted cases that had stretched on for over a year. We have expanded supervised release to ensure that those who can safely remain in the community before their trial do so, and are working to simplify the bail process. We have improved the summons process so that fewer people will end up with open warrants for minor crimes. Closing Rikers alone will not inherently stop an inmate with mental illness from lashing out when he hasn't received the healthcare he needs. It won't suddenly provide a hardworking officer with the safety equipment that will protect her throughout her shift. And it won?t automatically allow a young man who committed a low-level offense to safely wait for trial supervised in his community. These are reforms we must make and we are making no matter where our jails are located. Moving buildings alone is not the sweeping, comprehensive policy needed to make our jails safer and fairer. After decades of neglect, culture change won?t happen overnight. But we have signs that our reforms are starting to work. And no matter where we choose to house our jails ten years from now, today we need to keep moving down the path that is making the system better. For the first time ever, we are confronting important questions about Rikers Island and our criminal justice system as a whole in meaningful, realistic terms. The discussion about closing Rikers Island is an important one, and I welcome continued dialogue on this goal. But it cannot be the only goal. Because as New Yorkers and as progressives, we cannot merely relocate or rename a problem we must reform it, now.";
	char article[12101] = "";
	uint32 handle = 0;
	uint32 blknum = 0;
	
 	// General Test 1
 	printf("Test 1: A short story\n");
	if((handle = DfsInodeOpen(filename_short)) == DFS_FAIL){
		printf("RunOSTests cound't open inode.\n");
		GracefulExit();
	}
	if((blknum = DfsInodeAllocateVirtualBlock(handle, i)) == DFS_FAIL){
			printf("RunOSTests couldn't allocate virtual block.\n");
			GracefulExit();
	}
	if(DfsInodeWriteBytes(handle, (void*)story_short, 0, 524) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	if(DfsInodeReadBytes(handle, (void*)tale_short, 0, 524) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	DfsInodeDelete(handle);
	/*for(i = 0; i < 524; i++){
		printf("%c", tale_short[i]);
	}*/

	printf("Test 2: A long story\n");
	if((handle = DfsInodeOpen(filename_long)) == DFS_FAIL){
		printf("RunOSTests cound't open inode.\n");
		GracefulExit();
	}
	for(i = 0; i < 2; i++){
		if((blknum = DfsInodeAllocateVirtualBlock(handle, i)) == DFS_FAIL){
			printf("RunOSTests couldn't allocate virtual block.\n");
			GracefulExit();
		}
		printf("Virtual block allocated = %d\n", blknum);
	}
	if(DfsInodeWriteBytes(handle, (void*)story_long, 531, 1511) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	if(DfsInodeReadBytes(handle, (void*)tale_long, 531, 1511) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	DfsInodeDelete(handle);
	/*for(i = 0; i < 1511; i++){
		printf("%c", tale_long[i]);
	}*/

	printf("Test 3: A very long story\n");
	if((handle = DfsInodeOpen(filename_verylong)) == DFS_FAIL){
		printf("RunOSTests cound't open inode.\n");
		GracefulExit();
	}
	for(i = 0; i < 3; i++){
		if((blknum = DfsInodeAllocateVirtualBlock(handle, i)) == DFS_FAIL){
				printf("RunOSTests couldn't allocate virtual block.\n");
				GracefulExit();
		}
	}
	if(DfsInodeWriteBytes(handle, (void*)story_verylong, 0, 2263) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	if(DfsInodeReadBytes(handle, (void*)tale_verylong, 0, 2263) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	DfsInodeDelete(handle);
	/*for(i = 0; i < 1511; i++){
		printf("%c", tale_verylong[i]);
	}*/
	printf("Test 4: A long long story\n");
	if((handle = DfsInodeOpen(filename_passage)) == DFS_FAIL){
		printf("RunOSTests cound't open inode.\n");
		GracefulExit();
	}
	for(i = 0; i < 12; i++){
		if((blknum = DfsInodeAllocateVirtualBlock(handle, i)) == DFS_FAIL){
				printf("RunOSTests couldn't allocate virtual block.\n");
				GracefulExit();
		}
	}
	if(DfsInodeWriteBytes(handle, (void*)passage, 0, 12101) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	if(DfsInodeReadBytes(handle, (void*)article, 0, 12101) == DFS_FAIL){
		printf("RunOSTests coundn't write to file system.\n");
		GracefulExit();
	}
	DfsInodeDelete(handle);
	/*for(i = 0; i < 1511; i++){
		printf("%c", tale_verylong[i]);
	}*/
}

