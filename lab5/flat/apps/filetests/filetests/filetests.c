#include "usertraps.h"
#include "misc.h"

#include "filetests.h"
void main (int argc, char *argv[]){
  char story[2263] = "There is no shortage of tipsters around offering ‘get-rich-quick’ opportunities. But if you are a serious private investor, leave the Las Vegas mentality to those with money to fritter. The serious investor needs a proper ‘portfolio’ – a well-planned selection of investments, with a definite structure and a clear aim. But exactly how does a newcomer to the stock market go about achieving that? Well, if you go to five reputable stock brokers and ask them what you should do with your money, you’re likely to get five different answers, -- even if you give all the relevant information about your age, family, finances and what you want from your investments. Moral? There is no one ‘right’ way to structure a portfolio. However, there are undoubtedly some wrong ways, and you can be sure that none of our five advisers would have suggested sinking all (or perhaps any ) of your money into Periwigs. So what should you do? We’ll assume that you have sorted out the basics – like mortgages, pensions, insurance and access to sufficient cash reserves. You should then establish your own individual aims. These are partly a matter of personal circumstances, partly a matter of psychology. For instance, if you are older you have less time to recover from any major losses, and you may well wish to boost your pension income. So preserving your capital and generating extra income are your main priorities. In this case, you’d probably construct a portfolio with some shares(but not high risk ones), along with gifts, cash deposits, and perhaps convertibles or the income shares of split capital investment trusts. If you are younger, and in a solid financial position, you may decide to take an aggressive approach – but only if you ‘re blessed with a sanguine disposition and won’t suffer sleepless nights over share prices. If you recognize yourself in this description, you might include a couple of heady growth stocks in your portfolio, alongside your more pedestrian investments. Once you have decides on your investment aims, you can then decide where to put your money. The golden rule here is spread your risk – if you put all of your money into Periwigs International, you’re setting yourself up as a hostage to fortune. ";
  char tale[2263] = "";
  int bytes_read = 0;
  int bytes_write = 0;
  int fd = 0;
  int i = 0;
  int j = 0;

  if(argc != 1){
    Printf("Usage: %s. \n");
    Exit();
  }
  
  // Test 1 : Simple Write
  Printf("\nTest 1: Simple Write\n");
  if((fd = file_open("test1.txt", "w")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
    Exit();
  }
  if((bytes_write = file_write(fd, (void *)story, dstrlen(story))) == FILE_FAIL){
    Printf("main coudn't write to file test1.txt.\n");
    Exit();
  }
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
    Exit();
  }
  Printf("test1.txt: write %d bytes.\n", bytes_write);

  // Test 2: Simple Read
  Printf("\nTest 2: Simple Read\n");
  if((fd = file_open("test1.txt", "r")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
    Exit();
  }
  if((bytes_read = file_read(fd, (void *)tale, dstrlen(story))) == FILE_FAIL){
    Printf("main coudn't read from file test1.txt.\n");
    Exit();
  }
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
    Exit();
  }
  Printf("test1.txt: read %d bytes.\n", bytes_read);
  for(i = 0; i < bytes_read; i++){
    Printf("%c", tale[i]);
  }
  Printf("\n");
 	
  // Test 3: Read beyond the file -- EOF
  Printf("\nTest 3: Read beyond the file\n");
 	bzero(story, dstrlen(story));
  if((fd = file_open("test1.txt", "r")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
    Exit();
  }
  
  if((bytes_read = file_read(fd, (void *)story, dstrlen(tale) + 200)) == FILE_FAIL){
    Printf("main readched end of file.\n");
  }
	Printf("bytes_read = %d\n", bytes_read);
  for(i = 0; i < bytes_read; i++){
    Printf("%c", story[i]);
  }
  Printf("\n");
  bzero(tale, dstrlen(tale));

  Printf("Now read again without seek.\n");
  if((bytes_read = file_read(fd, (void *)tale, dstrlen(story))) == FILE_FAIL){
    Printf("main readched end of file.\n");
  }
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
    Exit();
  }

  // Test 4: Permission Test
  Printf("\nTest 4: File Permission Test\n");
  // Read mode
  if((fd = file_open("test1.txt", "r")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
    Exit();
  }
  if((bytes_write = file_write(fd, (void *)story, dstrlen(story))) == FILE_FAIL){
    Printf("main coudn't write to file test1.txt because read mode.\n");
  }
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
    Exit();
  }

  // Write mode
  if((fd = file_open("test1.txt", "w")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
    Exit();
  }
  if((bytes_read = file_read(fd, (void *)tale, dstrlen(story))) == FILE_FAIL){
    Printf("main coudn't read from file test1.txt because write mode.\n");
  }
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
    Exit();
  }
  // Unknown mode
  if((fd = file_open("test1.txt", "a")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
  }
  if((bytes_read = file_read(fd, (void *)tale, dstrlen(story))) == FILE_FAIL){
    Printf("main coudn't read from file test1.txt because unknown mode.\n");
  }
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
  }

  // R/W mode
  if((fd = file_open("test1.txt", "rw")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
    Exit();
  }
  if((bytes_write = file_write(fd, (void *)story, dstrlen(story))) == FILE_FAIL){
    Printf("main coudn't write to file test1.txt because read mode.\n");
  }
  if(file_seek(fd, -bytes_write, FILE_SEEK_CUR) == FILE_FAIL){
    Printf("main cound't set position.");
    Exit();
  }
  if((bytes_read = file_read(fd, (void *)tale, dstrlen(story))) == FILE_FAIL){
    Printf("main coudn't write to file test1.txt because unkonwn reason.\n");
    Exit();
  }
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
    Exit();
  }
  for(i = 0; i < dstrlen(tale); i++){
    Printf("%c", tale[i]);
  }
  Printf("\n");
  bzero(tale, dstrlen(tale));
  if(file_delete("test1.txt") == FILE_FAIL){
    Printf("main coudn't delete the file.\n");
    Exit();
  }

  // Test 5: File position Test
  Printf("\nTest 5: File Position Test\n");
  if((fd = file_open("test1.txt", "rw")) == FILE_FAIL){
    Printf("main coudn't open file test1.txt\n");
    Exit();
  }
  for(i = 0; i < 10; i++){
    if((bytes_write = file_write(fd, (void *)story, dstrlen(story))) == FILE_FAIL){
      Printf("main coudn't write to file test1.txt because read mode.\n");
      Exit();
    }
  }
  if(file_seek(fd, 0, FILE_SEEK_SET) == FILE_FAIL){
    Printf("main coudn't seek file to position.\n");
    Exit();
  }
  for(i = 0; i < 1131; i++){
    bytes_read = file_read(fd, (void*)tale, 20);
    for(j = 0; j < 20; j++){
      Printf("%c", tale[j]);
    }
  }
  Printf("\n");
  bzero(tale, dstrlen(tale));

  Printf("FileSeek Test Begin\n");

  if(file_seek(fd, 0, FILE_SEEK_SET) == FILE_FAIL){
    Printf("main coudn't seek file to position.\n");
    Exit();
  }
  bytes_read = file_read(fd, (void*)tale, 20);
  for(j = 0; j < 20; j++){
    Printf("%c", tale[j]);
  }
  Printf("\n");
  bzero(tale, dstrlen(tale));
  if(file_seek(fd, 20, FILE_SEEK_CUR) == FILE_FAIL){
    Printf("main coudn't seek file to position.\n");
    Exit();
  }

  bytes_read = file_read(fd, (void*)tale, 20);
  for(j = 0; j < 20; j++){
    Printf("%c", tale[j]);
  }
  Printf("\n");
  bzero(tale, dstrlen(tale));
  if(file_seek(fd, -30000, FILE_SEEK_END) == FILE_FAIL){
    Printf("main coudn't seek file to position.\n");
  }
  if(file_seek(fd, -20, FILE_SEEK_END) == FILE_FAIL){
    Printf("main coudn't seek file to position.\n");
  }
  bytes_read = file_read(fd, (void*)tale, 20);
  j = 0;
  while(tale[j] != '\0'){
    Printf("%c", tale[j]);
    j++;
  }
  Printf("\n");
  if(file_close(fd) == FILE_FAIL){
    Printf("main coudn't close the file.\n");
    Exit();
  }
  if(file_delete("test1.txt") == FILE_FAIL){
    Printf("main coudn't delete the file.\n");
    Exit();
  }
}
