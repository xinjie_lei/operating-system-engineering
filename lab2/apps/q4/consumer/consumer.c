#include "lab2-api.h"
#include "usertraps.h"
#include "misc.h"
#include "pc.h"

void main (int argc, char *argv[])
{
  pcsm *sm;        // Used to access shard circular buffer in shared memory page
  uint32 h_mem;            // Handle to the shared memory page
  lock_t mutex;            // Lock to control access to circular buffer
  cond_t notfull;          // Condition variable indicates the buffer is not full
  cond_t notempty;         // Condition variable indicates the buffer is not empty
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  int i;                   // Loop index

  if (argc != 6) { 
    Printf("Usage: "); Printf(argv[0]); Printf(" <handle_to_shared_memory_page> <handle_to_page_mapped_semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  h_mem = dstrtol(argv[1], NULL, 10); // The "10" means base 10
  mutex = dstrtol(argv[2], NULL, 10);
  notfull = dstrtol(argv[3], NULL, 10);
  notempty = dstrtol(argv[4], NULL, 10);
  s_procs_completed = dstrtol(argv[5], NULL, 10);

  // Map shared memory page into this process's memory space
  if ((sm = (pcsm *)shmat(h_mem)) == NULL) {
    Printf("Could not map the virtual address to the memory in "); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
 
  // Produce "Hello world" to buffer
  for(i = 0; i < 11; i++)
  {
    if(lock_acquire(mutex) != SYNC_SUCCESS) {
      Printf("Bad lock mutex (%d) in ", mutex); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }

    while(sm -> head == sm -> tail) {
      cond_wait(notempty);
    }

    Printf("Consumer %d removed: %c\n", getpid(), sm -> buf[sm -> tail]);
    sm -> tail = (sm -> tail + 1) % BUFFERSIZE;
    cond_signal(notfull);

    if(lock_release(mutex) != SYNC_SUCCESS){
      Printf("Bad lock mutex (%d) in ", mutex); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }
  }

  // Signal the semaphore to tell the original process that we're done
  //Printf("Consumer: PID %d is complete.\n", getpid());
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
}

