#ifndef __USERPROG__
#define __USERPROG__

#define PRODUCERH2O "producerH2O.dlx.obj"
#define PRODUCERSO4 "producerSO4.dlx.obj"
#define REACTIONH2O "reactionH2O.dlx.obj"
#define REACTIONSO4 "reactionSO4.dlx.obj"
#define REACTIONH2SO4 "reactionH2SO4.dlx.obj"

#endif
