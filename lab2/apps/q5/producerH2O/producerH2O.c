#include "lab2-api.h"
#include "usertraps.h"
#include "misc.h"
#include "reaction.h"

void main (int argc, char *argv[])
{
  int numH2O;
  sem_t H2O;
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  int i;                   // Loop index
  

  if (argc != 4) { 
    Printf("Usage: "); Printf(argv[0]); Printf("<number of H2O to be created> <H2O semaphore> <number of processes semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  numH2O = dstrtol(argv[1], NULL, 10);
  H2O = dstrtol(argv[2], NULL, 10);
  s_procs_completed = dstrtol(argv[3], NULL, 10);
 
  // Produce "Hello world" to buffer
  for(i = 0; i < numH2O; i++)
  {
    if(sem_signal(H2O) != SYNC_SUCCESS) {
      Printf("Bad semaphore H2O (%d) in ", H2O); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }
    Printf("Inject 1 H2O molecules.\n");
  }

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
}
