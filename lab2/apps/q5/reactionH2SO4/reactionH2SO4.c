#include "lab2-api.h"
#include "usertraps.h"
#include "misc.h"

void main (int argc, char *argv[])
{
  int min;
  sem_t SO2;
  sem_t H2;
  sem_t O2;
  sem_t H2SO4;
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  int i;                   // Loop index

  if (argc != 7) { 
    Printf("Usage: "); Printf(argv[0]); Printf(" <number of H2SO4 to be created> <SO2 semaphore> <H2 semaphore> <O2 semaphore> <H2SO4 semaphore> <number of processes semaphore>.\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  min = dstrtol(argv[1], NULL, 10); // The "10" means base 10
  SO2 = dstrtol(argv[2], NULL, 10);
  H2 = dstrtol(argv[3], NULL, 10);
  O2 = dstrtol(argv[4], NULL, 10);
  H2SO4 = dstrtol(argv[5], NULL, 10);
  s_procs_completed = dstrtol(argv[6], NULL, 10);
 
  // Produce "Hello world" to buffer
  for(i = 0; i < min; i++)
  {
    if(sem_wait(SO2) != SYNC_SUCCESS) {
      Printf("Bad semaphore SO2 (%d) in ", SO2); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }
    if(sem_wait(H2) != SYNC_SUCCESS) {
      Printf("Bad semaphore H2 (%d) in ", H2); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }
    if(sem_wait(O2) != SYNC_SUCCESS) {
      Printf("Bad semaphore O2 (%d) in ", O2); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }
    if(sem_signal(H2SO4) != SYNC_SUCCESS) {
      Printf("Bad semaphore H2SO4 (%d) in ", H2SO4); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }
    Printf("1 H2SO4 is created!\n");
  }

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
}

