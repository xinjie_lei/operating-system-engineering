#include "lab2-api.h"
#include "usertraps.h"
#include "misc.h"

void main (int argc, char *argv[])
{
  int numSO4;
  sem_t SO4;
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  int i;                   // Loop index
  

  if (argc != 4) { 
    Printf("Usage: "); Printf(argv[0]); Printf("<number of SO4 to be created> <SO4 semaphore> <number of processes semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  numSO4 = dstrtol(argv[1], NULL, 10);
  SO4 = dstrtol(argv[2], NULL, 10);
  s_procs_completed = dstrtol(argv[3], NULL, 10);
 
  // Produce "Hello world" to buffer
  for(i = 0; i < numSO4; i++)
  {
    if(sem_signal(SO4) != SYNC_SUCCESS) {
      Printf("Bad semaphore SO4 (%d) in ", SO4); Printf(argv[0]); Printf(", exiting...\n");
      Exit();
    }
    Printf("Inject 1 SO4 molecules.\n");
  }

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in ", s_procs_completed); Printf(argv[0]); Printf(", exiting...\n");
    Exit();
  }
}
