#ifndef __USERPROG__
#define __USERPROG__

typedef struct producer_consumer_shared_memory{
  int head;
  int tail;
  char buf[BUFFERSIZE];
} pcsm;

#define PRODUCER_TO_RUN "producer.dlx.obj"
#define CONSUMER_TO_RUN "consumer.dlx.obj"

#endif
