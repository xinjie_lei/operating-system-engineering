#include "usertraps.h"
#include "misc.h"

#define HELLO_WORLD "hello_world.dlx.obj"
#define BEYOND_MAX_VIR_ADDR "beyond_max_vir_addr.dlx.obj"
#define BEYOND_CURR_PAGE "beyond_curr_page.dlx.obj"
#define USTACK_1PAGE "ustack_1page.dlx.obj"
#define SPAWN "spawn.dlx.obj"

void main (int argc, char *argv[])
{
  int i;                               // Loop index variable
  sem_t s_procs_completed;             // Semaphore used to wait until all spawned processes have completed
  char s_procs_completed_str[10];      // Used as command-line argument to pass page_mapped handle to new processes

  if (argc != 1) {
    Printf("Usage: %s\n", argv[0]);
    Exit();
  }

  // Convert string from ascii command line argument to integer number
  Printf("makeprocs (%d): Creating 6 test processes in succession\n", getpid());

  // Create semaphore to not exit this process until all other processes 
  // have signalled that they are complete.
  if ((s_procs_completed = sem_create(0)) == SYNC_FAIL) {
    Printf("makeprocs (%d): Bad sem_create\n", getpid());
    Exit();
  }

  // Setup the command-line arguments for the new processes.  We're going to
  // pass the handles to the semaphore as strings
  // on the command line, so we must first convert them from ints to strings.
  ditoa(s_procs_completed, s_procs_completed_str);

  Printf("-------------------------------------------------------------------------------------\n");
  Printf("Test 1: Print Hello World messgae once.\n");
  process_create(HELLO_WORLD, s_procs_completed_str, NULL);
  if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
    Exit();
  }

  Printf("-------------------------------------------------------------------------------------\n");
  Printf("Test 2: Access address beyond current page.\n");
  process_create(BEYOND_CURR_PAGE, s_procs_completed_str, NULL);
  if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
    Exit();
  }

  Printf("-------------------------------------------------------------------------------------\n");
  Printf("Test 3: User stack grows beyond 1 page.\n");
  process_create(USTACK_1PAGE, s_procs_completed_str, NULL);
  if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
    Exit();
  }

  Printf("-------------------------------------------------------------------------------------\n");
  Printf("Test 4: Call Hello World 100 times in succession\n");
  for(i=0;i<100;i++){
    process_create(HELLO_WORLD, s_procs_completed_str, NULL);
    if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
      Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
      Exit();
    }
  }

  Printf("-------------------------------------------------------------------------------------\n");
  Printf("Test 5: Spawning 30 processes simutaneously\n");
  if ((s_procs_completed = sem_create(-29)) == SYNC_FAIL) {
    Printf("makeprocs (%d) test 6: Bad sem_create\n", getpid());
    Exit();
  }
  ditoa(s_procs_completed, s_procs_completed_str);
  for(i=0;i<30;i++){
    process_create(SPAWN, s_procs_completed_str, NULL);
  }
  if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
    Exit();
  }
  
  Printf("-------------------------------------------------------------------------------------\n");
  Printf("Test 6: Access address beyong max virtual address.\n");
  process_create(BEYOND_MAX_VIR_ADDR, s_procs_completed_str, NULL);
  if (sem_wait(s_procs_completed) != SYNC_SUCCESS) {
    Printf("Bad semaphore s_procs_completed (%d) in %s\n", s_procs_completed, argv[0]);
    Exit();
  }

  Printf("-------------------------------------------------------------------------------------\n");
  Printf("makeprocs (%d): All other processes completed, exiting main process.\n", getpid());

}
