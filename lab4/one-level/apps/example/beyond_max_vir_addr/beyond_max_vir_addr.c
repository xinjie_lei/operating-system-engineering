#include "usertraps.h"
#include "misc.h"
typedef unsigned int uint32;

void main (int argc, char *argv[])
{
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  uint32 *ptr=0x0;

  if (argc != 2) { 
    Printf("Usage: %s <handle_to_procs_completed_semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  s_procs_completed = dstrtol(argv[1], NULL, 10);

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("hello_world (%d): Bad semaphore s_procs_completed (%d)!\n", getpid(), s_procs_completed);
    Exit();
  }
  // Now assign a address(larger than max virtual address) to a pointer
  ptr = 0x3fffff + 1;
  printf("Content in %x = %d\n", ptr, *ptr);
  
  Printf("beyond_max_vir_addr (%d): Done!\n", getpid());
}
