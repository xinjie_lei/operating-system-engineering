#include "usertraps.h"
#include "misc.h"

typedef unsigned int uint32;

void main (int argc, char *argv[])
{
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
  uint32* addr1;
  uint32* addr2;
  uint32* addr3;
  uint32* addr4;
  int bytes1, bytes2, bytes3, bytes4; 

  if (argc != 2) { 
    Printf("Usage: %s <handle_to_procs_completed_semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  s_procs_completed = dstrtol(argv[1], NULL, 10);

  // Now print a message to show that everything worked
  addr1 = (uint32*)malloc(48);
  *addr1 = 3;

  addr2 = (uint32*)malloc(1000);
  *addr2 = 20;

  addr3 = (uint32*)malloc(1);
  *addr3 = 100;

  addr4 = (uint32*)malloc(2047);
  *addr4 = 80;

  bytes1 = mfree((void*)addr4);
  bytes2 = mfree((void*)addr3);
  bytes3 = mfree((void*)addr2);
  bytes4 = mfree((void*)addr1);

  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("hello_world (%d): Bad semaphore s_procs_completed (%d)!\n", getpid(), s_procs_completed);
    Exit();
  }

  Printf("Spawn (%d): Done!\n", getpid());
}
