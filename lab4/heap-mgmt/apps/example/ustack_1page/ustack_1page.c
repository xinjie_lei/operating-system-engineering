#include "usertraps.h"
#include "misc.h"

int sum(int a){
	if(a == 0){
		return 0;
	}
	else {
		return a + sum(a-1);
	}
}

void main (int argc, char *argv[])
{
  sem_t s_procs_completed; // Semaphore to signal the original process that we're done
	int total = 0;
  if (argc != 2) { 
    Printf("Usage: %s <handle_to_procs_completed_semaphore>\n"); 
    Exit();
  } 

  // Convert the command-line strings into integers for use as handles
  s_procs_completed = dstrtol(argv[1], NULL, 10);
  
  // Signal the semaphore to tell the original process that we're done
  if(sem_signal(s_procs_completed) != SYNC_SUCCESS) {
    Printf("hello_world (%d): Bad semaphore s_procs_completed (%d)!\n", getpid(), s_procs_completed);
    Exit();
  }
  // Now print a message to show that everything worked
  total = sum(2000);
  Printf("ustack_1page (%d): Done!\n", getpid());
}